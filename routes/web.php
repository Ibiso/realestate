<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@getIndex');
Route::get('about', 'PagesController@getAbout');
Route::get('contact', 'PagesController@getContact');
Route::get('view', 'PagesController@getView');
Route::resource('listing', 'ListingController');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/view/listing/{id}','PagesController@viewListing')->name('front.view.listing');

Route::group(['prefix'=>'admin','namespace'=>'Admin'],function(){
    
    Route::get('/login','LoginController@showLoginForm')->name('admin.get.loginForm');
    Route::post('/login','LoginController@login')->name('admin.login');
    Route::post('/logout','LoginController@logout')->name('admin.logout');

    Route::get('/dashboard','DashboardController@index')->name('admin.dashboard');

    Route::group(['prefix'=>'listings'],function(){
        Route::get('/index','ListingController@index')->name('admin.listings');
        Route::get('/show/{id}','ListingController@show')->name('admin.view.listings');
        Route::get('/create','ListingController@create')->name('admin.create.listings');
        Route::post('/store','ListingController@store')->name('admin.store.listings');
        Route::get('/edit/{id}','ListingController@edit')->name('admin.edit.listings');
        Route::post('/update/{id}','ListingController@update')->name('admin.update.listings');
        Route::post('/destroy/{id}','ListingController@destroy')->name('admin.destroy.listings');
    });


    Route::group(['prefix'=>'listings-category'],function(){
        Route::get('/index','ListingCategoryController@index')->name('admin.listings-cat');
        Route::post('/store','ListingCategoryController@store')->name('admin.store.listings-cat');
        Route::post('/update/{id}','ListingCategoryController@update')->name('admin.update.listings-cat');
        Route::post('/destroy/{id}','ListingCategoryController@destroy')->name('admin.destroy.listings-cat');
    });
});
