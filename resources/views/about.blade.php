@extends('layouts.real')

@section('content')
<section class="elementor-element elementor-element-6f86 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="6f86" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-2861 elementor-column elementor-col-100 elementor-top-column" data-id="2861" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-1fc8 elementor-widget elementor-widget-header-title" data-id="1fc8" data-element_type="widget" data-widget_type="header-title.default">
                            <div class="elementor-widget-container">
                                <h2 class="vis-hid">Header elementor</h2>

                                <section class="pager-sec bfr " style="background-image: url(../content/themes/ogarent/assets/images/resources/pager-bg.jpg);">
                                    <div class="container">
                                        <div class="pager-sec-details">
                                            <h3 class="">About</h3>
                                            <ul>
                                                <li class="item">
                                                    <a href="../index.html">Home</a>
                                                </li>
                                                <li>
                                                    <span>Pages</span>
                                                </li>
                                                <li>
                                                    <span>About</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--pager-sec-details end-->
                                    </div>
                                </section>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="elementor-element elementor-element-699d elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="699d" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-7074 elementor-column elementor-col-100 elementor-top-column" data-id="7074" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-47 elementor-widget elementor-widget-about-features" data-id="47" data-element_type="widget" data-widget_type="about-features.default">
                            <div class="elementor-widget-container">

                                <h2 class="vis-hid">Serices elementor</h2>
                                <section class="sec-about-features section-padding  zebra-section ">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-6">
                                                <div class="section-heading">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-about">
                                            <div class="item">
                                                <div class="icon"><i class="fas fa-check"></i></div>
                                                <div class="body">
                                                    <h3 class="title">Real Estate Solutions</h3>
                                                    <div class="descr">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit conseat ipsum, nec sagittis sem nibh.</div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="icon"><i class="fas fa-check"></i></div>
                                                <div class="body">
                                                    <h3 class="title">Renting and Booking</h3>
                                                    <div class="descr">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit conseat ipsum, nec sagittis sem nibh.</div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="icon"><i class="fas fa-check"></i></div>
                                                <div class="body">
                                                    <h3 class="title">Awesome Support</h3>
                                                    <div class="descr">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit conseat ipsum, nec sagittis sem nibh.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="../content/themes/ogarent/assets/images/placeholder-about.jpg" alt="" class="cover-sec-about-features">
                                </section>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="elementor-element elementor-element-69ad elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="69ad" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-70d4 elementor-column elementor-col-100 elementor-top-column" data-id="70d4" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-472 elementor-widget elementor-widget-carousel-purposes" data-id="472" data-element_type="widget" data-widget_type="carousel-purposes.default">
                            <div class="elementor-widget-container">

                                <h2 class="vis-hid">Serices elementor</h2>
                                <section class="sect-carousel-purposes section-padding">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-6">
                                                <div class="section-heading">
                                                    <span class="title">Find More</span>

                                                    <h3 class="title">Areas We Cover</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Iq4qASI1Fok" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                                        </iframe>
                                        <section class="explore-feature hp_s1 section-padding section-services-you-need  ">
                                            <div class="container">
                                                <div class="row justify-content-center">
                                                    <div class="col-xl-6">
                                                        <div class="section-heading">
                                                            <span class="">Find More</span>

                                                            <h3 class="">Our Main Features</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                                                        <a href="#" title="">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <i class="la la-home"></i>
                                                                    <h3 class="">Perfect Tools</h3>
                                                                    <p class="">Duis sed odio sit amet nibh vulputate cursus ametmauris. Morbi accumsan ipsum velit. Name tellus a odio tincidunt auctor a ornare odio.</p>
                                                                </div>
                                                            </div>
                                                            <!--card end-->
                                                        </a>
                                                    </div>
                                                    <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                                                        <a href="#" title="">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <i class="icon-cursor"></i>
                                                                    <h3 class="">Search In Click</h3>
                                                                    <p class="">Duis sed odio sit amet nibh vulputate cursus ametmauris. Morbi accumsan ipsum velit. Name tellus a odio tincidunt auctor a ornare odio.</p>
                                                                </div>
                                                            </div>
                                                            <!--card end-->
                                                        </a>
                                                    </div>
                                                    <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                                                        <a href="#" title="">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <i class="icon-lock"></i>
                                                                    <h3 class="">User Control</h3>
                                                                    <p class="">Duis sed odio sit amet nibh vulputate cursus ametmauris. Morbi accumsan ipsum velit. Name tellus a odio tincidunt auctor a ornare odio.</p>
                                                                </div>
                                                            </div>
                                                            <!--card end-->
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </section>


                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="elementor-element elementor-element-1230 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="1230" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-59e1 elementor-column elementor-col-100 elementor-top-column" data-id="59e1" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-7e52 elementor-widget elementor-widget-map-svg-area" data-id="7e52" data-element_type="widget" data-widget_type="map-svg-area.default">
                            <div class="elementor-widget-container">

                                <script>
                                    var treefields = [];
                                    treefields["23"] = [];
                                    treefields["23"]['id'] = "23";
                                    treefields["23"]['name'] = "United States";
                                    treefields["23"]['count'] = "29";
                                    treefields["23"]['parent_id'] = "0";

                                    treefields["24"] = [];
                                    treefields["24"]['id'] = "24";
                                    treefields["24"]['name'] = "New York";
                                    treefields["24"]['count'] = "4";
                                    treefields["24"]['parent_id'] = "23";

                                    treefields["25"] = [];
                                    treefields["25"]['id'] = "25";
                                    treefields["25"]['name'] = "Florida";
                                    treefields["25"]['count'] = "5";
                                    treefields["25"]['parent_id'] = "23";

                                    treefields["26"] = [];
                                    treefields["26"]['id'] = "26";
                                    treefields["26"]['name'] = "Massachusetts";
                                    treefields["26"]['count'] = "3";
                                    treefields["26"]['parent_id'] = "23";

                                    treefields["27"] = [];
                                    treefields["27"]['id'] = "27";
                                    treefields["27"]['name'] = "Minnesota";
                                    treefields["27"]['count'] = "6";
                                    treefields["27"]['parent_id'] = "23";

                                    treefields["28"] = [];
                                    treefields["28"]['id'] = "28";
                                    treefields["28"]['name'] = "Montana";
                                    treefields["28"]['count'] = "0";
                                    treefields["28"]['parent_id'] = "23";

                                    treefields["29"] = [];
                                    treefields["29"]['id'] = "29";
                                    treefields["29"]['name'] = "North Dakota";
                                    treefields["29"]['count'] = "0";
                                    treefields["29"]['parent_id'] = "23";

                                    treefields["30"] = [];
                                    treefields["30"]['id'] = "30";
                                    treefields["30"]['name'] = "Hawaii";
                                    treefields["30"]['count'] = "1";
                                    treefields["30"]['parent_id'] = "23";

                                    treefields["31"] = [];
                                    treefields["31"]['id'] = "31";
                                    treefields["31"]['name'] = "Idaho";
                                    treefields["31"]['count'] = "0";
                                    treefields["31"]['parent_id'] = "23";

                                    treefields["32"] = [];
                                    treefields["32"]['id'] = "32";
                                    treefields["32"]['name'] = "Washington";
                                    treefields["32"]['count'] = "1";
                                    treefields["32"]['parent_id'] = "23";

                                    treefields["33"] = [];
                                    treefields["33"]['id'] = "33";
                                    treefields["33"]['name'] = "Arizona";
                                    treefields["33"]['count'] = "0";
                                    treefields["33"]['parent_id'] = "23";

                                    treefields["34"] = [];
                                    treefields["34"]['id'] = "34";
                                    treefields["34"]['name'] = "California";
                                    treefields["34"]['count'] = "0";
                                    treefields["34"]['parent_id'] = "23";

                                    treefields["35"] = [];
                                    treefields["35"]['id'] = "35";
                                    treefields["35"]['name'] = "Colorado";
                                    treefields["35"]['count'] = "0";
                                    treefields["35"]['parent_id'] = "23";

                                    treefields["36"] = [];
                                    treefields["36"]['id'] = "36";
                                    treefields["36"]['name'] = "Nevada";
                                    treefields["36"]['count'] = "0";
                                    treefields["36"]['parent_id'] = "23";

                                    treefields["37"] = [];
                                    treefields["37"]['id'] = "37";
                                    treefields["37"]['name'] = "New Mexico";
                                    treefields["37"]['count'] = "1";
                                    treefields["37"]['parent_id'] = "23";

                                    treefields["38"] = [];
                                    treefields["38"]['id'] = "38";
                                    treefields["38"]['name'] = "Oregon";
                                    treefields["38"]['count'] = "0";
                                    treefields["38"]['parent_id'] = "23";

                                    treefields["39"] = [];
                                    treefields["39"]['id'] = "39";
                                    treefields["39"]['name'] = "Utah";
                                    treefields["39"]['count'] = "0";
                                    treefields["39"]['parent_id'] = "23";

                                    treefields["40"] = [];
                                    treefields["40"]['id'] = "40";
                                    treefields["40"]['name'] = "Wyoming";
                                    treefields["40"]['count'] = "2";
                                    treefields["40"]['parent_id'] = "23";

                                    treefields["41"] = [];
                                    treefields["41"]['id'] = "41";
                                    treefields["41"]['name'] = "Arkansas";
                                    treefields["41"]['count'] = "0";
                                    treefields["41"]['parent_id'] = "23";

                                    treefields["42"] = [];
                                    treefields["42"]['id'] = "42";
                                    treefields["42"]['name'] = "Iowa";
                                    treefields["42"]['count'] = "1";
                                    treefields["42"]['parent_id'] = "23";

                                    treefields["43"] = [];
                                    treefields["43"]['id'] = "43";
                                    treefields["43"]['name'] = "Kansas";
                                    treefields["43"]['count'] = "0";
                                    treefields["43"]['parent_id'] = "23";

                                    treefields["44"] = [];
                                    treefields["44"]['id'] = "44";
                                    treefields["44"]['name'] = "Missouri";
                                    treefields["44"]['count'] = "0";
                                    treefields["44"]['parent_id'] = "23";

                                    treefields["45"] = [];
                                    treefields["45"]['id'] = "45";
                                    treefields["45"]['name'] = "Nebraska";
                                    treefields["45"]['count'] = "0";
                                    treefields["45"]['parent_id'] = "23";

                                    treefields["46"] = [];
                                    treefields["46"]['id'] = "46";
                                    treefields["46"]['name'] = "Oklahoma";
                                    treefields["46"]['count'] = "1";
                                    treefields["46"]['parent_id'] = "23";

                                    treefields["47"] = [];
                                    treefields["47"]['id'] = "47";
                                    treefields["47"]['name'] = "South Dakota";
                                    treefields["47"]['count'] = "0";
                                    treefields["47"]['parent_id'] = "23";

                                    treefields["48"] = [];
                                    treefields["48"]['id'] = "48";
                                    treefields["48"]['name'] = "Louisiana";
                                    treefields["48"]['count'] = "0";
                                    treefields["48"]['parent_id'] = "23";

                                    treefields["49"] = [];
                                    treefields["49"]['id'] = "49";
                                    treefields["49"]['name'] = "Texas";
                                    treefields["49"]['count'] = "0";
                                    treefields["49"]['parent_id'] = "23";

                                    treefields["50"] = [];
                                    treefields["50"]['id'] = "50";
                                    treefields["50"]['name'] = "Connecticut";
                                    treefields["50"]['count'] = "0";
                                    treefields["50"]['parent_id'] = "23";

                                    treefields["51"] = [];
                                    treefields["51"]['id'] = "51";
                                    treefields["51"]['name'] = "New Hampshire";
                                    treefields["51"]['count'] = "0";
                                    treefields["51"]['parent_id'] = "23";

                                    treefields["52"] = [];
                                    treefields["52"]['id'] = "52";
                                    treefields["52"]['name'] = "Rhode Island";
                                    treefields["52"]['count'] = "0";
                                    treefields["52"]['parent_id'] = "23";

                                    treefields["53"] = [];
                                    treefields["53"]['id'] = "53";
                                    treefields["53"]['name'] = "Vermont";
                                    treefields["53"]['count'] = "0";
                                    treefields["53"]['parent_id'] = "23";

                                    treefields["54"] = [];
                                    treefields["54"]['id'] = "54";
                                    treefields["54"]['name'] = "Alabama";
                                    treefields["54"]['count'] = "1";
                                    treefields["54"]['parent_id'] = "23";

                                    treefields["55"] = [];
                                    treefields["55"]['id'] = "55";
                                    treefields["55"]['name'] = "Georgia";
                                    treefields["55"]['count'] = "0";
                                    treefields["55"]['parent_id'] = "23";

                                    treefields["56"] = [];
                                    treefields["56"]['id'] = "56";
                                    treefields["56"]['name'] = "Mississippi";
                                    treefields["56"]['count'] = "0";
                                    treefields["56"]['parent_id'] = "23";

                                    treefields["57"] = [];
                                    treefields["57"]['id'] = "57";
                                    treefields["57"]['name'] = "South Carolina";
                                    treefields["57"]['count'] = "0";
                                    treefields["57"]['parent_id'] = "23";

                                    treefields["58"] = [];
                                    treefields["58"]['id'] = "58";
                                    treefields["58"]['name'] = "Illinois";
                                    treefields["58"]['count'] = "0";
                                    treefields["58"]['parent_id'] = "23";

                                    treefields["59"] = [];
                                    treefields["59"]['id'] = "59";
                                    treefields["59"]['name'] = "Indiana";
                                    treefields["59"]['count'] = "0";
                                    treefields["59"]['parent_id'] = "23";

                                    treefields["60"] = [];
                                    treefields["60"]['id'] = "60";
                                    treefields["60"]['name'] = "Kentucky";
                                    treefields["60"]['count'] = "0";
                                    treefields["60"]['parent_id'] = "23";

                                    treefields["61"] = [];
                                    treefields["61"]['id'] = "61";
                                    treefields["61"]['name'] = "North Carolina";
                                    treefields["61"]['count'] = "0";
                                    treefields["61"]['parent_id'] = "23";

                                    treefields["62"] = [];
                                    treefields["62"]['id'] = "62";
                                    treefields["62"]['name'] = "Ohio";
                                    treefields["62"]['count'] = "0";
                                    treefields["62"]['parent_id'] = "23";

                                    treefields["63"] = [];
                                    treefields["63"]['id'] = "63";
                                    treefields["63"]['name'] = "Tennessee";
                                    treefields["63"]['count'] = "0";
                                    treefields["63"]['parent_id'] = "23";

                                    treefields["64"] = [];
                                    treefields["64"]['id'] = "64";
                                    treefields["64"]['name'] = "Virginia";
                                    treefields["64"]['count'] = "0";
                                    treefields["64"]['parent_id'] = "23";

                                    treefields["65"] = [];
                                    treefields["65"]['id'] = "65";
                                    treefields["65"]['name'] = "Wisconsin";
                                    treefields["65"]['count'] = "0";
                                    treefields["65"]['parent_id'] = "23";

                                    treefields["66"] = [];
                                    treefields["66"]['id'] = "66";
                                    treefields["66"]['name'] = "West Virginia";
                                    treefields["66"]['count'] = "0";
                                    treefields["66"]['parent_id'] = "23";

                                    treefields["67"] = [];
                                    treefields["67"]['id'] = "67";
                                    treefields["67"]['name'] = "Delaware";
                                    treefields["67"]['count'] = "0";
                                    treefields["67"]['parent_id'] = "23";

                                    treefields["68"] = [];
                                    treefields["68"]['id'] = "68";
                                    treefields["68"]['name'] = "District of Columbia";
                                    treefields["68"]['count'] = "0";
                                    treefields["68"]['parent_id'] = "23";

                                    treefields["69"] = [];
                                    treefields["69"]['id'] = "69";
                                    treefields["69"]['name'] = "Maryland";
                                    treefields["69"]['count'] = "0";
                                    treefields["69"]['parent_id'] = "23";

                                    treefields["70"] = [];
                                    treefields["70"]['id'] = "70";
                                    treefields["70"]['name'] = "New Jersey";
                                    treefields["70"]['count'] = "2";
                                    treefields["70"]['parent_id'] = "23";

                                    treefields["71"] = [];
                                    treefields["71"]['id'] = "71";
                                    treefields["71"]['name'] = "Pennsylvania";
                                    treefields["71"]['count'] = "0";
                                    treefields["71"]['parent_id'] = "23";

                                    treefields["72"] = [];
                                    treefields["72"]['id'] = "72";
                                    treefields["72"]['name'] = "Maine";
                                    treefields["72"]['count'] = "1";
                                    treefields["72"]['parent_id'] = "23";

                                    treefields["73"] = [];
                                    treefields["73"]['id'] = "73";
                                    treefields["73"]['name'] = "Michigan";
                                    treefields["73"]['count'] = "0";
                                    treefields["73"]['parent_id'] = "23";

                                    treefields["74"] = [];
                                    treefields["74"]['id'] = "74";
                                    treefields["74"]['name'] = "Alaska";
                                    treefields["74"]['count'] = "0";
                                    treefields["74"]['parent_id'] = "23";

                                    /*
                                     * Set item in geo menu
                                     * @param dataPath (string) value-path for treefield field ("Croatia - Zagreb")
                                     *
                                     */

                                    /*
                                     *
                                     * Styles for svg
                                     *
                                     */
                                    var svg_default_area_color = '#d2d2d2' /* default color*/
                                    var svg_selected_area_color = '#6a7be7' /* selected color*/
                                    var svg_hover_area_color = '#6a7ae7' /* hover color*/
                                    var svg_selected_country_color = '#d2d2d2';
                                    var svg_stroke_color = '#fff';
                                    var firstload = true;

                                    var geo_trigger_treefield = false;

                                    function set_path(dataPath, apply_in_search, tree_field) {
                                        if (typeof apply_in_search === 'undefined') apply_in_search = true;
                                        if (typeof tree_field === 'undefined') tree_field = true;
                                        var dataPath_origin = dataPath;
                                        // refresh

                                        var s_values_splited = new Array();
                                        s_values_splited.push(dataPath);
                                        var recurse_path = function(treefields, dataPath) {
                                            if (typeof treefields[dataPath] !== 'undefined' && treefields[dataPath]['parent_id'] != '0') {
                                                var id = treefields[treefields[dataPath]['parent_id']]['id'];
                                                s_values_splited.push(id);
                                                if (typeof treefields[treefields[dataPath]['parent_id']] !== 'undefined' && treefields[treefields[dataPath]['parent_id']]['parent_id'] != '0') {
                                                    recurse_path(treefields, id);
                                                }
                                            }
                                        }
                                        recurse_path(treefields, dataPath)

                                        var _last_element = jQuery.trim(s_values_splited[s_values_splited.length - 1]);

                                        jQuery('.geo-menu li').removeClass('active');
                                        jQuery('.geo-menu ul > li li').css('display', 'none');
                                        jQuery('.geo-menu ul > li').css('display', 'inline-block');
                                        jQuery('.geo-menu ul a').css('display', 'block')

                                        var _dataPath = '';
                                        jQuery.each(s_values_splited, function(key, val) {
                                            if (key > 1) return false;
                                            /*console.log('key: '+key, 'val: '+val)*/

                                            val = jQuery.trim(val);
                                            if (!jQuery('.geo-menu a[data-id="' + val + '"]').length) return false;

                                            var _this = jQuery('.geo-menu a[data-id="' + val + '"]');
                                            var parent = _this.closest('li');

                                            if (parent.hasClass('active')) {
                                                parent.removeClass('active')
                                                return false;
                                            }

                                            parent.addClass('active')
                                            if (parent.find('li').length) {
                                                jQuery(' > li', parent.parent()).not(parent).css('display', 'none');
                                                _this.css('display', 'none')
                                                jQuery(' li', parent).css('display', 'inline-block');
                                                jQuery('.geo-menu ul' + _this.attr('href')).show();
                                            }
                                        })

                                        if (apply_in_search) {
                                            if (dataPath_origin != '')
                                                jQuery('.search_option_2').val(dataPath_origin);
                                            else
                                                jQuery('.search_option_2').val('');
                                        }

                                        if (apply_in_search && tree_field && jQuery('.group_location_id .winter_dropdown_tree').length) {
                                            var dropdown = jQuery('.group_location_id .winter_dropdown_tree');
                                            /*dropdown.find('input').val(dataPath);*/
                                            geo_trigger_treefield = true;
                                            dropdown.find('.list_items li[key="' + dataPath + '"]').trigger('click');
                                            geo_trigger_treefield = false;
                                        }


                                        /* short-more tags*/
                                        dataPath_origin = jQuery.trim(s_values_splited)

                                        if (firstload && dataPath_origin[dataPath_origin.length - 1] == '-') {
                                            dataPath_origin = dataPath_origin.slice(0, -1);
                                            dataPath_origin = jQuery.trim(dataPath_origin)
                                        }

                                        var selector = jQuery.trim(s_values_splited[(s_values_splited.length - 1)]);

                                        if (jQuery('a[data-id="' + selector + '"]').closest('li').find('ul li .more-tags').length && jQuery('a[data-id="' + selector + '"]').closest('li').find('ul li .more-tags').attr('data-close') == 'false') {} else {
                                            ogarent_hideShow_tags(selector);
                                        }
                                        firstload = false;
                                    }

                                    /* menu geo */
                                    jQuery(document).ready(function($) {

                                        // if search_option_$treefield_id input missing
                                        /*if(!$('.search_option_2').length) {
                                            $('.sw_search_primary').append('<input type="text" class="hidden form-control search_option_2" name="search_option_2" value="" id="search_option_2">')
                                        }*/

                                        $('.geo-menu a').on('click', function(e) {
                                            e.preventDefault();
                                            var dataPath = $(this).attr('data-id') || '';

                                            if ($(this).parent().hasClass('active')) {
                                                dataPath = '';
                                                $(this).parent().removeClass('active');
                                                if (jQuery('.group_location_id .winter_dropdown_tree').length) {
                                                    var dropdown = jQuery('.group_location_id .winter_dropdown_tree');
                                                    geo_trigger_treefield = true;
                                                    dropdown.find('.list_items li[key=""]').trigger('click');
                                                    geo_trigger_treefield = false;
                                                } else {
                                                    jQuery('.search_option_2').val(dataPath)
                                                }
                                                return;
                                            }
                                            set_path(dataPath)
                                        })

                                    })

                                    /* additional methds for svg map */
                                    jQuery.fn.myAddClass = function(classTitle) {
                                        return this.each(function() {
                                            var oldClass = jQuery(this).attr("class") || '';
                                            oldClass = oldClass ? oldClass : '';
                                            jQuery(this).attr("class", (oldClass + " " + classTitle).trim());
                                        });
                                    }
                                    jQuery.fn.myRemoveClass = function(classTitle) {
                                        return this.each(function() {
                                            var oldClass = jQuery(this).attr("class") || '';
                                            var newClass = oldClass.replace(classTitle, '');
                                            jQuery(this).attr("class", newClass.trim());
                                        });
                                    }
                                    jQuery.fn.myHasClass = function(classTitle) {
                                        var current_class = jQuery(this).attr("class") || '';
                                        if (current_class.indexOf(classTitle) == '-1') {
                                            return false;
                                        } else {
                                            return true;
                                        }
                                    }

                                    // map
                                    jQuery(window).load(function($) {
                                        $ = jQuery
                                        if ($('#svgmap').length) {

                                            var nameAreaRoot = false;
                                            var trigger = false;
                                            var first_load_map = true;

                                            var svgobject = document.getElementById('svgmap');
                                            if ('contentDocument' in svgobject) {
                                                var svgdom = jQuery(svgobject.contentDocument);
                                            }
                                            /* colors */
                                            $('*', svgdom).css('stroke', svg_stroke_color);
                                            $('*', svgdom).css('stroke-width', '3px');
                                            $('*[data-idtreefield]', svgdom).not('.area').css({
                                                'fill': svg_default_area_color,
                                                'transition': 'fill .6s',
                                                '-webkit-transtion': 'fill .6s'
                                            });
                                            /* end colors */

                                            $('.treefield-tags a:not(#geo-menu-back)').on('click', function() {

                                                // country hover
                                                if ($('.geo-menu .treefield-tags >li.active >a').attr('data-id-lvl_0'))
                                                    $('*[data-id-lvl_0="' + $('.geo-menu .treefield-tags >li.active >a').attr('data-id-lvl_0').trim() + '"]:not(.highlight)', svgdom).css('fill', svg_selected_country_color);

                                                if ($(this).attr('data-id')) {
                                                    if ($('*[data-idtreefield="' + $(this).attr('data-id').trim() + '"]', svgdom).length) {
                                                        trigger = true
                                                        $('*[data-idtreefield="' + $(this).attr('data-id').trim() + '"]', svgdom).trigger('click');

                                                    } else {
                                                        $('*[data-idtreefield]', svgdom).myRemoveClass('highlight');
                                                    }
                                                } else {
                                                    $('*[data-idtreefield]', svgdom).myRemoveClass('highlight');
                                                }
                                            })

                                            $('.geo-menu #geo-menu-back').on('click', function(e) {
                                                e.preventDefault();
                                                $('*[data-idtreefield]', svgdom).myRemoveClass('highlight');
                                                $('*[data-idtreefield]', svgdom).not('.area').css('fill', svg_default_area_color);
                                            })

                                            /* start selected area */
                                            $('*[data-idtreefield]', svgdom).on('click', function() {

                                                if ($(this).myHasClass('highlight')) {
                                                    $('*[data-idtreefield]', svgdom).myRemoveClass('highlight');
                                                    $('*[data-idtreefield]', svgdom).not('.area').css('fill', svg_default_area_color);

                                                    if (!trigger && $(this).attr('data-idtreefield')) {
                                                        var dataPath = $(this).attr('data-idtreefield');
                                                        set_path(dataPath);
                                                    }
                                                    return false;
                                                }

                                                $('*[data-idtreefield]', svgdom).myRemoveClass('highlight');
                                                $('*[data-idtreefield]', svgdom).not('.area').css('fill', svg_default_area_color);

                                                /* highlight country */
                                                $('*[data-name-lvl_0="' + $(this).attr('data-name-lvl_0').trim() + '"]', svgdom).css('fill', svg_selected_country_color);

                                                $(this).myAddClass('highlight');
                                                if (!$(this).myHasClass('area'))
                                                    $(this).css('fill', svg_selected_area_color);
                                                if (!trigger && $(this).attr('data-name-lvl_1') && $(this).attr('data-idtreefield').trim()) {
                                                    var dataPath = $(this).attr('data-idtreefield');
                                                    set_path(dataPath);
                                                }

                                                trigger = false;
                                            })
                                            /* end selected area */

                                            $('*[data-idtreefield]', svgdom).on({
                                                'mouseover': function(e) {
                                                    if (!$(this).myHasClass('highlight') && !$(this).myHasClass('area'))
                                                        $(this).css('fill', svg_hover_area_color);
                                                },
                                                'mouseout': function(e) {
                                                    if (!$(this).myHasClass('highlight') && !$(this).myHasClass('area'))
                                                        $(this).css('fill', svg_default_area_color);

                                                    if ($('.geo-menu .treefield-tags >li.active >a').attr('data-id-lvl_0'))
                                                        $('*[data-id-lvl_0="' + $('.geo-menu .treefield-tags >li.active >a').attr('data-id-lvl_0').trim() + '"]:not(.highlight)', svgdom).css('fill', svg_selected_country_color);
                                                }
                                            });
                                            /* end hover area */

                                            // init map first load with data
                                            if (first_load_map) {
                                                var init_dataPath = '' || '23' || '';
                                                ogarent_hideShow_tags(init_dataPath);

                                                /* fix proporties svg file from amcharts */
                                                var attr = $('svg', svgdom).attr('xmlns:amcharts');
                                                if (typeof attr !== typeof undefined && attr !== false) {
                                                    /*console.log($('svg', svgdom).find('g'));*/
                                                    var _h = $('svg', svgdom).find('g').get(0).getBoundingClientRect().height || 500;
                                                    var _w = $('svg', svgdom).find('g').get(0).getBoundingClientRect().width || 1000;
                                                    $('svg', svgdom).attr('viewBox', '0 0 ' + _w + ' ' + (_h + 10) + '');
                                                }

                                                /* end fix proporties svg file */

                                                var dataPath = '' || '23' || '';
                                                $('.group_location_id input[name="search_location"]').trigger('change');
                                                set_path(dataPath, false);
                                            }
                                            first_load_map = false;

                                            /* start hint */

                                            $('*[data-idtreefield]', svgdom).on({
                                                'mouseover': function(e) {
                                                    var textHin = '';
                                                    $(this).css('cursor', 'pointer');
                                                    var id = $(this).attr('data-idtreefield').trim()
                                                    if (typeof treefields[id] != 'undefined') {
                                                        textHint = '<span class="location">' + treefields[id].name + '</span>';

                                                        if (treefields[id].count) {
                                                            if (treefields[id].count == 1)
                                                                textHint += ' ' + ' <span class="count">' + treefields[id].count + '<br/>Listing</span>';
                                                            else
                                                                textHint += ' ' + ' <span class="count">' + treefields[id].count + '<br/>Listings</span>';

                                                        }
                                                    } else {
                                                        return false;
                                                    }


                                                    $('body').append('<div id="map-geo-tooltip-alt">' + textHint + '</div>')

                                                    var mapCoord = document.getElementById("svgmap").getBoundingClientRect();
                                                    $(this).mouseover(function() {
                                                        $('#map-geo-tooltip-alt').css({
                                                            opacity: 1,
                                                            display: "none"
                                                        }).fadeIn(200);
                                                    }).mousemove(function(kmouse) {
                                                        var max_right = mapCoord.right - 150;
                                                        if (max_right < kmouse.pageX)
                                                            $('#map-geo-tooltip-alt').css({
                                                                left: 'initial',
                                                                right: mapCoord.right - kmouse.pageX - 32,
                                                                top: mapCoord.top + kmouse.pageY - 65
                                                            });
                                                        else
                                                            $('#map-geo-tooltip-alt').css({
                                                                right: 'initial',
                                                                left: mapCoord.left + kmouse.pageX - 32,
                                                                top: mapCoord.top + kmouse.pageY - 65
                                                            });
                                                    });
                                                    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                                                        setTimeout(function() {
                                                            $('#map-geo-tooltip-alt').fadeOut(100).remove();
                                                        }, 1000);
                                                    }
                                                },
                                                'mouseout': function() {
                                                    $('#map-geo-tooltip-alt').fadeOut(100).remove();
                                                },
                                            });
                                            /* end hint */
                                        }
                                    })
                                </script>

                                <script>
                                    // change dropdown tree if exist
                                    jQuery(document).ready(function($) {

                                        $('.group_location_id input[name="search_location"]').change(function(e, trigger) {
                                            if (geo_trigger_treefield && false) {
                                                $('.sw-search-start').trigger('click');
                                                return false;
                                            }
                                            var id_region = $(this).val();
                                            set_path(id_region, true, false);
                                            /* start selected area */

                                            if ($('#svgmap').length && id_region) {
                                                var svgobject = document.getElementById('svgmap');
                                                if ('contentDocument' in svgobject) {
                                                    var svgdom = jQuery(svgobject.contentDocument);
                                                }
                                                $('*[data-idtreefield]', svgdom).myRemoveClass('highlight');

                                                $('*[data-idtreefield]', svgdom).not('.area').css('fill', svg_default_area_color);

                                                $('*[data-idtreefield="' + id_region + '"]', svgdom).myAddClass('highlight');
                                                $('*[data-idtreefield="' + id_region + '"]', svgdom).not('.area').css('fill', svg_selected_area_color);
                                            } else if ($('#svgmap').length) {
                                                var svgobject = document.getElementById('svgmap');
                                                if ('contentDocument' in svgobject) {
                                                    var svgdom = jQuery(svgobject.contentDocument);
                                                }
                                                $('*[data-idtreefield]', svgdom).myRemoveClass('highlight');

                                                $('*[data-idtreefield]', svgdom).not('.area').css('fill', svg_default_area_color);

                                            }
                                            /* end selected area */



                                        })
                                    })
                                </script>

                                <script>
                                    /* for first load */
                                    jQuery(document).ready(function($) {
                                        var dataPath = '' || '23' || '';
                                        set_path(dataPath, false);
                                    })


                                    function ogarent_hideShow_tags(parent_seletor) {
                                        if (typeof parent_seletor === 'undefined') return false;
                                        if (jQuery('.geo-menu a[data-id="' + parent_seletor + '"]').closest('li').find('ul li').length > 18) {
                                            var _Liselector = jQuery('.geo-menu a[data-id="' + parent_seletor + '"]').closest('li').find('ul li');
                                            var _count = 0;

                                            if (_Liselector.hasClass('active'))
                                                _count = 1;

                                            jQuery.each(_Liselector, function(key, value) {
                                                if (!jQuery(this).hasClass('active') && !jQuery(this).find('a').hasClass('more-tags') && _count > 18) {
                                                    jQuery(this).css('display', 'none');
                                                } else {
                                                    jQuery(this).css('display', 'inline-block');
                                                }
                                                if (!jQuery(this).hasClass('active'))
                                                    _count++;
                                            })

                                            if (!jQuery('.geo-menu a[data-id="' + parent_seletor + '"]').closest('li').find('ul li .more-tags').length) {
                                                jQuery('<li> <a href="#" class="more-tags c-base" data-close="true">more</a></li>').appendTo(jQuery('.geo-menu a[data-id="' + parent_seletor + '"]').closest('li').find('ul')).find('.more-tags').on('click', function() {
                                                    if (jQuery(this).attr('data-close') == 'true') {
                                                        jQuery(this).closest('ul').find('li').css('display', 'inline-block');
                                                        jQuery(this).attr('data-close', 'false').html('short')
                                                    } else if (jQuery(this).attr('data-close') == 'false') {
                                                        ogarent_hideShow_tags(parent_seletor);
                                                    }
                                                })
                                            } else {
                                                jQuery('a[data-id="' + parent_seletor + '"]').closest('li').find('ul li .more-tags').attr('data-close', 'true').html('more')
                                            }
                                        } else {}
                                    }
                                </script>

                                <h2 class="vis-hid">Svg elementor</h2>
                                <section class="map-svg-area-sec section-padding    no-padding">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-6">
                                                <div class="section-heading">
                                                    <span>Find More</span>

                                                    <h3>Areas We Cover</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="map-details">
                                            <div class="map-geo-widget">
                                                <div class="row">
                                                    <div class="col-md-6 d-none">
                                                        <div class="geo-menu">
                                                            <ul class="treefield-tags">
                                                                <li class=''><a href="#United-States" data-id-lvl_0="23" data-id='23'>United States</a>
                                                                    <ul class='' id="United-States">
                                                                        <li><a href="#" id='geo-menu-back' data-path=''> <i class="fa fa-arrow-left"></i> back </a></li>

                                                                        <li><a href="#" title="New York" data-region='New York' data-id-lvl_0="23" data-id-lvl_1="24" data-id='24'>New York <span class="item-count">(4)</span></a>
                                                                        <li><a href="#" title="Florida" data-region='Florida' data-id-lvl_0="23" data-id-lvl_1="25" data-id='25'>Florida <span class="item-count">(5)</span></a>
                                                                        <li><a href="#" title="Massachusetts" data-region='Massachusetts' data-id-lvl_0="23" data-id-lvl_1="26" data-id='26'>Massachusetts <span class="item-count">(3)</span></a>
                                                                        <li><a href="#" title="Minnesota" data-region='Minnesota' data-id-lvl_0="23" data-id-lvl_1="27" data-id='27'>Minnesota <span class="item-count">(6)</span></a>
                                                                        <li><a href="#" title="Montana" data-region='Montana' data-id-lvl_0="23" data-id-lvl_1="28" data-id='28'>Montana <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="North Dakota" data-region='North Dakota' data-id-lvl_0="23" data-id-lvl_1="29" data-id='29'>North Dakota <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Hawaii" data-region='Hawaii' data-id-lvl_0="23" data-id-lvl_1="30" data-id='30'>Hawaii <span class="item-count">(1)</span></a>
                                                                        <li><a href="#" title="Idaho" data-region='Idaho' data-id-lvl_0="23" data-id-lvl_1="31" data-id='31'>Idaho <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Washington" data-region='Washington' data-id-lvl_0="23" data-id-lvl_1="32" data-id='32'>Washington <span class="item-count">(1)</span></a>
                                                                        <li><a href="#" title="Arizona" data-region='Arizona' data-id-lvl_0="23" data-id-lvl_1="33" data-id='33'>Arizona <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="California" data-region='California' data-id-lvl_0="23" data-id-lvl_1="34" data-id='34'>California <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Colorado" data-region='Colorado' data-id-lvl_0="23" data-id-lvl_1="35" data-id='35'>Colorado <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Nevada" data-region='Nevada' data-id-lvl_0="23" data-id-lvl_1="36" data-id='36'>Nevada <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="New Mexico" data-region='New Mexico' data-id-lvl_0="23" data-id-lvl_1="37" data-id='37'>New Mexico <span class="item-count">(1)</span></a>
                                                                        <li><a href="#" title="Oregon" data-region='Oregon' data-id-lvl_0="23" data-id-lvl_1="38" data-id='38'>Oregon <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Utah" data-region='Utah' data-id-lvl_0="23" data-id-lvl_1="39" data-id='39'>Utah <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Wyoming" data-region='Wyoming' data-id-lvl_0="23" data-id-lvl_1="40" data-id='40'>Wyoming <span class="item-count">(2)</span></a>
                                                                        <li><a href="#" title="Arkansas" data-region='Arkansas' data-id-lvl_0="23" data-id-lvl_1="41" data-id='41'>Arkansas <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Iowa" data-region='Iowa' data-id-lvl_0="23" data-id-lvl_1="42" data-id='42'>Iowa <span class="item-count">(1)</span></a>
                                                                        <li><a href="#" title="Kansas" data-region='Kansas' data-id-lvl_0="23" data-id-lvl_1="43" data-id='43'>Kansas <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Missouri" data-region='Missouri' data-id-lvl_0="23" data-id-lvl_1="44" data-id='44'>Missouri <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Nebraska" data-region='Nebraska' data-id-lvl_0="23" data-id-lvl_1="45" data-id='45'>Nebraska <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Oklahoma" data-region='Oklahoma' data-id-lvl_0="23" data-id-lvl_1="46" data-id='46'>Oklahoma <span class="item-count">(1)</span></a>
                                                                        <li><a href="#" title="South Dakota" data-region='South Dakota' data-id-lvl_0="23" data-id-lvl_1="47" data-id='47'>South Dakota <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Louisiana" data-region='Louisiana' data-id-lvl_0="23" data-id-lvl_1="48" data-id='48'>Louisiana <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Texas" data-region='Texas' data-id-lvl_0="23" data-id-lvl_1="49" data-id='49'>Texas <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Connecticut" data-region='Connecticut' data-id-lvl_0="23" data-id-lvl_1="50" data-id='50'>Connecticut <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="New Hampshire" data-region='New Hampshire' data-id-lvl_0="23" data-id-lvl_1="51" data-id='51'>New Hampshire <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Rhode Island" data-region='Rhode Island' data-id-lvl_0="23" data-id-lvl_1="52" data-id='52'>Rhode Island <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Vermont" data-region='Vermont' data-id-lvl_0="23" data-id-lvl_1="53" data-id='53'>Vermont <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Alabama" data-region='Alabama' data-id-lvl_0="23" data-id-lvl_1="54" data-id='54'>Alabama <span class="item-count">(1)</span></a>
                                                                        <li><a href="#" title="Georgia" data-region='Georgia' data-id-lvl_0="23" data-id-lvl_1="55" data-id='55'>Georgia <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Mississippi" data-region='Mississippi' data-id-lvl_0="23" data-id-lvl_1="56" data-id='56'>Mississippi <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="South Carolina" data-region='South Carolina' data-id-lvl_0="23" data-id-lvl_1="57" data-id='57'>South Carolina <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Illinois" data-region='Illinois' data-id-lvl_0="23" data-id-lvl_1="58" data-id='58'>Illinois <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Indiana" data-region='Indiana' data-id-lvl_0="23" data-id-lvl_1="59" data-id='59'>Indiana <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Kentucky" data-region='Kentucky' data-id-lvl_0="23" data-id-lvl_1="60" data-id='60'>Kentucky <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="North Carolina" data-region='North Carolina' data-id-lvl_0="23" data-id-lvl_1="61" data-id='61'>North Carolina <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Ohio" data-region='Ohio' data-id-lvl_0="23" data-id-lvl_1="62" data-id='62'>Ohio <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Tennessee" data-region='Tennessee' data-id-lvl_0="23" data-id-lvl_1="63" data-id='63'>Tennessee <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Virginia" data-region='Virginia' data-id-lvl_0="23" data-id-lvl_1="64" data-id='64'>Virginia <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Wisconsin" data-region='Wisconsin' data-id-lvl_0="23" data-id-lvl_1="65" data-id='65'>Wisconsin <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="West Virginia" data-region='West Virginia' data-id-lvl_0="23" data-id-lvl_1="66" data-id='66'>West Virginia <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Delaware" data-region='Delaware' data-id-lvl_0="23" data-id-lvl_1="67" data-id='67'>Delaware <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="District of Columbia" data-region='District of Columbia' data-id-lvl_0="23" data-id-lvl_1="68" data-id='68'>District of Columbia <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Maryland" data-region='Maryland' data-id-lvl_0="23" data-id-lvl_1="69" data-id='69'>Maryland <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="New Jersey" data-region='New Jersey' data-id-lvl_0="23" data-id-lvl_1="70" data-id='70'>New Jersey <span class="item-count">(2)</span></a>
                                                                        <li><a href="#" title="Pennsylvania" data-region='Pennsylvania' data-id-lvl_0="23" data-id-lvl_1="71" data-id='71'>Pennsylvania <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Maine" data-region='Maine' data-id-lvl_0="23" data-id-lvl_1="72" data-id='72'>Maine <span class="item-count">(1)</span></a>
                                                                        <li><a href="#" title="Michigan" data-region='Michigan' data-id-lvl_0="23" data-id-lvl_1="73" data-id='73'>Michigan <span class="item-count">(0)</span></a>
                                                                        <li><a href="#" title="Alaska" data-region='Alaska' data-id-lvl_0="23" data-id-lvl_1="74" data-id='74'>Alaska <span class="item-count">(0)</span></a>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div id="map-geo">
                                                            <object data="../content/uploads/sw_win/files/current_map.svg" type="image/svg+xml" id="svgmap" width="500" height="360"></object>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--map-details end-->
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="elementor-element elementor-element-313f elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="313f" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-695e elementor-column elementor-col-100 elementor-top-column" data-id="695e" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-299 elementor-widget elementor-widget-slogan" data-id="299" data-element_type="widget" data-widget_type="slogan.default">
                            <div class="elementor-widget-container">
                                <h2 class="vis-hid">Slogan elementor</h2>

                                <a href="#" class="section-slogan" title="">
                                    <section class="cta st2 section-padding" style="background-color:#303e94">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="cta-text">
                                                        <h2 class="">Discover a home you'll love to stay</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </a>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="elementor-element elementor-element-3d6a elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="3d6a" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-2f55 elementor-column elementor-col-100 elementor-top-column" data-id="2f55" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-5a0c elementor-widget elementor-widget-placeholder-footer" data-id="5a0c" data-element_type="widget" data-widget_type="placeholder-footer.default">
                            <div class="elementor-widget-container">

                                <h2 class="vis-hid">Footer elementor</h2>

                                <section class="bottom section-padding">
                                    <div class="container placeholder-container">
                                        <div class="row">
                                            <div id="widget-logo-social-1" class="widget footer-regular widget-logo-social block-ogarent col-xl-3 col-sm-6 col-md-4">
                                                <div class="bottom-logo">
                                                    <a href="../index.html">
                                                        <img src="../content/themes/ogarent/assets/images/logo.png" alt="" class="img-fluid">
                                                    </a>
                                                    <div class="content">
                                                        <p class="description">
                                                            Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat co nsequat auctor eu in elit.</p>
                                                    </div>

                                                </div>
                                            </div>
                                            <div id="widget-footer-contacts-1" class="widget footer-regular widget-footer-contacts block-ogarent col-xl-3 col-sm-6 col-md-3">
                                                <div class="widget-footer-contacts">
                                                    <h3>Contact Us</h3>
                                                    <ul class="footer-list">
                                                        <li><i class="la la-map-marker"></i> <span class="value">432 Park Ave, New York, NY 10022</span></li>
                                                        <li><i class="la la-phone"></i> <span class="value"><a href="tel:(844)%20380-8603">(844) 380-8603</a></span></li>
                                                        <li><i class="la la-envelope"></i> <span class="value"><a href="mailto:support@ogarent.com">support@ogarent.com</a></span></li>
                                                        <li>
                                                        <li>
                                                        <li><i class="la la-chevron-circle-right"></i><span class="value"><a href="#">Contact Us</a></span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div id="widget-follow-us-1" class="widget footer-regular widget-follow-us block-ogarent col-xl-6 col-sm-12 col-md-5">
                                                <div class="bottom-list widget-follow-us">
                                                    <h3>Follow Us</h3>
                                                    <div class="footer-social"><a href="https://www.facebook.com/share.php?u=https%3A%2F%2Flisting-themes.com%2Fogarent-wp%2Fabout" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/home?status=https%3A%2F%2Flisting-themes.com%2Fogarent-wp%2Fabout" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter"></i></a><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Flisting-themes.com%2Fogarent-wp%2Fabout&amp;title=&amp;summary=&amp;source=" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-linkedin"></i></a><a href="https://www.instagram.com/" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-instagram"></i></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="../content/themes/ogarent/assets/images/placeholder-footer.png" alt="placeholder" class="footer-placeholder">
                                    </div>
                                </section>
                                @endsection