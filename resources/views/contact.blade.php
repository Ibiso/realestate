@extends('layouts.real')

@section('content')

				<section class="elementor-element elementor-element-44711 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="44711" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-62318 elementor-column elementor-col-100 elementor-top-column" data-id="62318" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-61f4b elementor-widget elementor-widget-contact-featured" data-id="61f4b" data-element_type="widget" data-widget_type="contact-featured.default">
				<div class="elementor-widget-container">
			
<h2 class="vis-hid">Serices elementor</h2>
<section class="sect-contact-featured section-padding   no-padding">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6">
                                			</div>
		</div>
		<div class="row justify-content-center row-contact">
                <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                    <div class="contact-card">
                        <div class="logo">
                            <h2 class="title">
                                <i class="la la-location-arrow"></i><span class="text elementor-inline-editing">Selio</span>
                            </h2>
                            <span class="mini-title elementor-inline-editing">Selio Group Headquarters</span>
                        </div>
                        <div class="address elementor-inline-editing">130 5th Avenue<br/>Floor 6<br/>New York, NY 10011</div>
                    </div><!--card end-->
                </div>
                    
                    
                <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                        <div class="contact-card featured">
                            <img src="../wp-content/themes/selio/assets/img/contact-featured-bg.jpg" alt="" class="cover">
                            <h2 class="title elementor-inline-editing">I’m agent broker</h2>
                            <a href="#" class="btn-default">Request info</a>
                        </div><!--card end-->
                </div>
                    
                <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                        <div class="contact-card">
                            <div class="contact_info">
                                <h3 class="elementor-inline-editing">Other Contact Information</h3>
                                <ul class="cont_info">
                                                                                <li><i class="la la-map-marker"></i> <span class="address elementor-inline-editing">212 5th Ave, New York</span></li>
                                                                                                                        <li><i class="la la-phone"></i> <span class=" elementor-inline-editing">+1 212-925-3797</span></li>
                                                                                                                        <li><i class="la la-envelope"></i><a href="mailto:tomas@contact.com" title="" >tomas@contact.com</a></li>
                                                                        </ul>
                                <ul class="social_links">
                                                                                        <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
                                                                                                                        <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                                                                                                                        <li><a href="#" title=""><i class="fa fa-instagram"></i></a></li>
                                                                                                                        <li><a href="#" title=""><i class="fa fa-linkedin"></i></a></li>
                                                                        </ul>
                            </div><!--contact_info end-->
                        </div><!--card end-->
                </div>
                    
		</div>
	</div>
</section>


				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-7c4 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="7c4" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-22d7 elementor-column elementor-col-100 elementor-top-column" data-id="22d7" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-4256 elementor-widget elementor-widget-contact-section" data-id="4256" data-element_type="widget" data-widget_type="contact-section.default">
				<div class="elementor-widget-container">
			

<div class="contact-sec">
<div class="container">
	<div class="contact-details-sec">
		<div class="row">
					<div class="col-lg-8 col-md-8 pl-0">
						<div class="contact_form" id="contact_form_169">
					<h3 class="">Get in Touch</h3>
                                         					<form action="#contact_form_169" class="js-ajax-form" method="POST">
						<div class="form-group no-pt">
							
							
						</div><!--form-group end-->
						<div class="form-fieldss">
							<div class="row">
								<div class="col-lg-4 col-md-4 pl-0">
									<div class="form-field">
										<input type="text" name="your_name" value="" placeholder="Your Name" id="your_name" class="">
									</div><!-- form-field end-->
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-field">
										<input type="email" name="mail" value="" placeholder="Your Email" id="mail" class="">
									</div><!-- form-field end-->
								</div>
								<div class="col-lg-4 col-md-4 pr-0">
									<div class="form-field">
										<input type="text" name="phone" value="" placeholder="Your Phone" class="">
									</div><!-- form-field end-->
								</div>
								<div class="col-lg-12 col-md-12 pl-0 pr-0">
									<div class="form-field">
										<textarea name="message" placeholder="Your Message" class=""></textarea>
									</div><!-- form-field end-->
								</div>
                                                                								<div class="col-lg-12 col-md-12 pl-0">
									<button type="submit" class="btn-default submit" >Send Message</button>
								</div>
								
							</div>
						</div><!--form-fieldss end-->
					</form>
				</div><!--contact_form end-->
			</div>
						<div class="col-lg-4 col-md-4 pr-0">
				<div class="contact_info">
                                        					<ul class="cont_info">
																							</ul>
                                                                                
                                                                                    <div class="text"><span class="">Aenean sollicitudin, lorem quis bibendum auctor, nisi elitco nsequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accum sanpsum velit. Nam nec tellus a odio tinci.</span></div>
                                        				</div><!--contact_info end-->
			</div>
					</div>
	</div><!--contact-details-sec end-->
	<br style="clear:both;" />
</div>
<br style="clear:both;" />
</div><!--contact-sec end-->

				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-1573 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="1573" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-2de2 elementor-column elementor-col-100 elementor-top-column" data-id="2de2" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-36ad elementor-widget elementor-widget-slogan" data-id="36ad" data-element_type="widget" data-widget_type="slogan.default">
				<div class="elementor-widget-container">
			<h2 class="vis-hid">Slogan elementor</h2>

<a href="#" class="section-slogan" title="">
	<section class="cta st2 section-padding" style="background-color:#303e94">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="cta-text">
						<h2 class="">Discover a home you'll love to stay</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
</a>


				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
@endsection