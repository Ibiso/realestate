
<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="UTF-8">
		<title>ogarent</title>
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="ogarent &raquo; Feed" href="feed/index.php" />
<link rel="alternate" type="application/rss+xml" title="ogarent &raquo; Comments Feed" href="comments/feed/index.php" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/listing-themes.com\/ogarent-wp\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.11"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='ogarent-front-ui-css'  href="{{ asset('content/plugins/SW_Win_ogarent_Admin_Ui/css/front-ui433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-animate-css'  href="{{ asset('content/themes/ogarent/assets/css/animate.min433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='slick-css'  href="{{ asset('content/themes/ogarent/assets/js/lib/slick/slick433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='slick-theme-css'  href="{{ asset('content/themes/ogarent/assets/js/lib/slick/slick-theme433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='fontawesome-5-css'  href="{{ asset('content/themes/ogarent/assets/libraries/fontawesome-5.8/css/fontawesome-5433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href="{{ asset('content/plugins/real1/assets/lib/font-awesome/css/font-awesome.min1849.css?ver=4.7.0')}}" type='text/css' media='all' />
<link rel='stylesheet' id='simple-line-icons-css'  href="{{ asset('content/themes/ogarent/assets/icons/simple-line-icons/css/simple-line-icons433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='line-awesome-css'  href="{{ asset('content/themes/ogarent/assets/icons/simple-line-icons/css/line-awesome.min433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href="{{ asset('content/themes/ogarent/assets/css/bootstrap.min433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-style-css'  href="{{ asset('content/themes/ogarent/assets/css/style433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-responsive-css'  href="{{ asset('content/themes/ogarent/assets/css/responsive433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-color-css'  href="{{ asset('content/themes/ogarent/assets/css/color433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-fonts-css'  href='http://fonts.googleapis.com/css?family=Lora%7COpen+Sans:300,400,600,700%7CPlayfair+Display:400,700%7CPoppins:300,400,500,600,700%7CRaleway:300,400,500,600,700,800%7CRoboto:300,400,500,700&amp;display=swap&amp;subset=cyrillic&amp;display=swap' type='text/css' media='all' />
<link rel='stylesheet' id='ionicons-css'  href="{{ asset('content/themes/ogarent/assets/css/ionicons.min8aee.css?ver=4.1.2')}}" type='text/css' media='all' />
<link rel='stylesheet' id='blueimp-gallery-css'  href="{{ asset('content/themes/ogarent/assets/css/blueimp-gallery.min433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='nouislider-css'  href="{{ asset('content/themes/ogarent/assets/libraries/nouislider/nouislider433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='card-slider-style-css'  href="{{ asset('content/themes/ogarent/assets/libraries/card-slider/dist/css/style433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-wp-style-css'  href="{{ asset('content/themes/ogarent/assets/css/wp-style433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-helper-form-css'  href="{{ asset('content/themes/ogarent/assets/css/helper-form433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='ogarent-custom-css'  href="{{ asset('content/themes/ogarent/assets/css/custom433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<style id='ogarent-custom-inline-css' type='text/css'>

        .bootstrap-datetimepicker-widget .glyphicon-time:before {
            content: "Hourly Booking";
        }

        .bootstrap-datetimepicker-widget .glyphicon-calendar:before {
            content:  "Switch to dates";
        }

</style>
<link rel='stylesheet' id='ogarent-custom-media-css'  href="{{ asset('content/themes/ogarent/assets/css/custom-media433d.css?ver=4.9.11')}}" type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href="{{ asset('content/plugins/real1/assets/lib/eicons/css/elementor-icons.min9a8d.css?ver=5.3.0')}}" type='text/css' media='all' />
<link rel='stylesheet' id='elementor-animations-css'  href="{{ asset('content/plugins/real1/assets/lib/animations/animations.min91ac.css?ver=2.6.8')}}" type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href="{{ asset('content/plugins/real1/assets/css/frontend.min91ac.css?ver=2.6.8')}}" type='text/css' media='all' />
<link rel='stylesheet' id='elementor-global-css'  href="{{ asset('content/uploads/elementor/css/global36da.css?ver=1569510809')}}" type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=4.9.11' type='text/css' media='all' />
<script type='text/javascript' src="{{ asset('includes/js/jquery/jqueryb8ff.js?ver=1.12.4')}}"></script>
<script type='text/javascript' src="{{ asset('includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script>
<script type='text/javascript' src="{{ asset('content/plugins/SW_Win_Classified/assets/js/front-custom-scripts8a54.js?ver=1.0.0')}}"></script>
<script type='text/javascript'>

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = {{ asset('../../connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.5')}};
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

</script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/bootstrap.min2f54.js?ver=4.1')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/affixef85.js?ver=3.7')}}></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.9.11" />
<link rel='shortlink' href='index.php' />
<meta property="og:url" content="https://listing-themes.com/ogarent-wp" />
<meta property="og:type" content="page" />
<meta property="og:title"  content="ogarent &#8211; Just another WordPress site" />
<meta name="description" content="Just another WordPress site">
<meta property="og:description"  content="Just another WordPress site" />
<link rel="canonical" href="index.php" />
<link rel="icon" href="../content/uploads/2019/09/favicon.png" sizes="32x32" />
<link rel="icon" href="../content/uploads/2019/09/favicon.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="content/uploads/2019/09/favicon.png" />
<meta name="msapplication-TileImage" content="https://listing-themes.com/ogarent-wp/content/uploads/2019/09/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /></head>
<body class="home page-template page-template-elementor_canvas page page-id-79 wide elementor-default elementor-template-canvas elementor-page elementor-page-79">
			<div data-elementor-type="post" data-elementor-id="79" class="elementor elementor-79" data-elementor-settings="[]">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
							<section class="elementor-element elementor-element-edb elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="edb" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-bea elementor-column elementor-col-100 elementor-top-column" data-id="bea" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-27d5 elementor-widget elementor-widget-top-bar" data-id="27d5" data-element_type="widget" data-widget_type="top-bar.default">
				<div class="elementor-widget-container">
			<h2 class="vis-hid">Top bar elementor</h2>
<div class="top-header section-top-bar  d-none d-sm-block">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-xl-6 col-md-7 col-sm-12">
                <div class="header-address">
                                                                    <a href="tel://%28647%29+346-0855">
                            <i class="la la-phone-square"></i>
                            <span>+234 902 619 0497</span>
                        </a>
                                                                         <a href="#">
                            <i class="la la-map-marker"></i>
                            <span>No 8 Ehoro Cresent </span>
                        </a>
                                                            </div>
            </div>
                        <div class="col-xl-3 col-md-5 col-sm-12">
                                            <div class="header-social">
                     <a href="https://www.facebook.com/share.php?u=https://listing-themes.com/ogarent-wp" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/home?status=https://listing-themes.com/ogarent-wp" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter"></i></a><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://listing-themes.com/ogarent-wp&amp;title=&amp;summary=&amp;source=" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-linkedin"></i></a><a href="https://www.instagram.com/" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-instagram"></i></a>                </div>
                            </div>

                    </div>
    </div>
</div>


				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
<section class="elementor-element elementor-element-7cb6 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="7cb6" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-6c51 elementor-column elementor-col-100 elementor-top-column" data-id="6c51" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-3d78 elementor-widget elementor-widget-top-navigation-white" data-id="3d78" data-element_type="widget" data-widget_type="top-navigation-white.default">
				<div class="elementor-widget-container">
			<h2 class="vis-hid">Nav elementor</h2>
<div class="header section-top-navigation-white">
<div class="container">
	<div class="row">
		<div class="col-xl-12">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand" href="index.php">
                                    					<img src="content/themes/ogarent/assets/images/logo.png" style="width: 50px; height: 50px;" alt="">
                                    				</a>
				<button class="menu-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent">
					<span class="icon-spar"></span>
					<span class="icon-spar"></span>
					<span class="icon-spar"></span>
				</button>

				<div class="navbar-collapse" id="navbarSupportedContent">

						<ul id="top-menu" class="navbar-nav mr-auto">
								<li class="" style="margin-top: -4px;margin-right: 34px;"><a href="{{ url('/')}}" class="dropdown-item">Home</a></li>
                            <li class="" style="margin-top: -4px;margin-right: 34px;"><a href="{{ url('about')}}" class="dropdown-item">About</a></li>
								<li class="" style="margin-top: -4px;margin-right: 34px;"><a href="{{ url('view')}}" class="dropdown-item">Listings</a></li>
								<li class="" style="margin-top: -4px;margin-right: 34px;"><a href="{{ url('contact')}}" class="dropdown-item">Contact</a></li>
					</ul>
					<div class="d-inline my-2 my-lg-0">
					</div>
					<a href="#" title="" class="close-menu"><i class="la la-close"></i></a>
				</div>
			</nav>
		</div>
	</div>
</div>
</div>


				</div>
				</div>
						</div>
			</div>
		</div>
	</div>
			</div>
</section>
@yield('content')
<section class="bottom section-padding">
<div class="container placeholder-container">
<div class="row">
 <div id="widget-logo-social-1" class="widget footer-regular widget-logo-social block-ogarent col-xl-3 col-sm-6 col-md-4">
<div class="bottom-logo">
		 <a href="index.html">
				 <img src="{{ asset('content/themes/ogarent/assets/images/logo.png')}}" alt="" class="img-fluid">
		 </a>
<div class="content">
<p class="description">
 Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat co nsequat auctor eu in elit.
</p>
</div>
</div></div>
<div id="widget-footer-contacts-1" class="widget footer-regular widget-footer-contacts block-ogarent col-xl-3 col-sm-6 col-md-3">
<div class="widget-footer-contacts">
<h3>Contact Us</h3>
<ul class="footer-list">
<li><i class="la la-map-marker"></i><span class="value">432 Park Ave, New York, NY 10022</span></li>
<li><i class="la la-phone"></i> <span class="value"><a href="tel:(844)%20380-8603">(844) 380-8603</a></span></li>
<li><i class="la la-envelope"></i> <span class="value"><a href="mailto:support@ogarent.com">support@ogarent.com</a></span></li>
<li><li><li><i class="la la-chevron-circle-right"></i><span class="value"><a href="#">Contact Us</a></span></li>
</ul>
</div>
</div>
<div id="widget-follow-us-1" class="widget footer-regular widget-follow-us block-ogarent col-xl-6 col-sm-12 col-md-5">
<div class="bottom-list widget-follow-us">
<h3>Follow Us</h3>
<div class="footer-social">
 <a href="#"><i class="fa fa-facebook"></i></a>
 <a href="#"><i class="fa fa-twitter"></i></a>
 <a href="#"><i class="fa fa-linkedin"></i></a>
 <a href="#"><i class="fa fa-instagram"></i></a>
</div>
</div>
</div>
</div>
<img src="{{ asset('content/themes/ogarent/assets/images/placeholder-footer.png')}}" alt="placeholder" class="footer-placeholder">
</div>
</section>

<footer class="footer">
<div class="container">
<div class="row">
 <div class="col-xl-12">
		 <div class="footer-content">
				 <div class="row justify-content-between">
						 <div class="col-xl-12">
								 <div class="copyright text-center">
										 <p>&copy; ogarent theme made in EU. All Rights Reserved.</p>
								 </div>
						 </div>
				 </div>
		 </div>
 </div>
</div>
</div>
</footer>

</div>
</div>
 </div>
</div>
</div>
 </div>
</div>
</section>
 </div>
</div>
</div>

<link rel='stylesheet' id='sw_win_basic_bootstrap-css'  href={{ asset('content/plugins/SW_Win_Classified/assets/css/basic-bootstrap-wrapper8a54.css?ver=1.0.0')}} type='text/css' media='all' />
<link rel='stylesheet' id='sw_win_font_awesome-css'  href={{ asset('content/plugins/SW_Win_Classified/assets/css/font-awesome.min8a54.css?ver=1.0.0')}} type='text/css' media='all' />
<link rel='stylesheet' id='leaflet-maps-api-css'  href={{ asset('../../unpkg.com/leaflet%401.3.3/dist/leaflet.css?ver=4.9.11')}} type='text/css' media='all' />
<link rel='stylesheet' id='leaflet-maps-api-cluster-def-css'  href={{ asset('../../unpkg.com/leaflet.markercluster%401.3.0/dist/MarkerCluster.Default.css?ver=4.9.11')}} type='text/css' media='all' />
<link rel='stylesheet' id='leaflet-maps-api-cluster-css'  href={{ asset('../../unpkg.com/leaflet.markercluster%401.3.0/dist/MarkerCluster.css?ver=4.9.11')}} type='text/css' media='all' />
<script type='text/javascript' src={{ asset('content/plugins/real1-ogarent/assets/js/screenshot433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/libraries/nouislider/nouislider433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/libraries/card-slider/dist/js/card-slider-min433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/modernizr-3.6.0.min5152.js?ver=1.0')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/scripts5152.js?ver=1.0')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/blueimp-gallery433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/lib/slick/slick433d.js?ver=4.9.11')}}></script>
<script type='text/javascript'>

</script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/html5lightbox433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/facebook433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/ogarent-drop-menu433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src='https://maps.google.com/maps/api/js?key&amp;ver=4.9.11'></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/map-cluster/infobox.min433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/markerclusterer433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/map-cluster/maps433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/sw-custom-marker433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/map-cluster/raphael433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/map-cluster/jquery.usmap433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/jquery.helpers433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/themes/ogarent/assets/js/custom433d.js?ver=4.9.11')}}></script>
<script type='text/javascript'>

var default_marker_url = {{ asset('content/themes/ogarent/assets/images/markers/default.png')}};

})
</script>
<script type='text/javascript' src={{ asset('includes/js/wp-embed.min433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/SW_Win_Classified/assets/js/typeahead/bootstrap3-typeahead433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/SW_Win_Classified/assets/js/winter_treefield/winter433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/SW_Win_Classified/assets/js/script433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/SW_Win_Classified/assets/js/editable_table/jquery.tabledit.min433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/SW_Win_Classified/assets/js/bootstrap_carousel.min433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/SW_Win_Classified/assets/js/jquery.helpers8a54.js?ver=1.0.0')}}></script>
<script type='text/javascript' src={{ asset('../../unpkg.com/leaflet%401.3.3/dist/leaflet.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('../../unpkg.com/leaflet.markercluster%401.3.0/dist/leaflet.markercluster.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/SW_Win_Classified/assets/js/sw_win_open_map_obj433d.js?ver=4.9.11')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/real1/assets/js/frontend-modules.min91ac.js?ver=2.6.8')}}></script>
<script type='text/javascript' src={{ asset('includes/js/jquery/ui/position.mine899.js?ver=1.11.4')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/real1/assets/lib/dialog/dialog.minfe9d.js?ver=4.7.3')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/real1/assets/lib/waypoints/waypoints.min05da.js?ver=4.0.2')}}></script>
<script type='text/javascript' src={{ asset('content/plugins/real1/assets/lib/swiper/swiper.min4f24.js?ver=4.4.6')}}></script>
<script type='text/javascript'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"2.6.8","urls":{"assets":"https:\/\/ogarent.com.ng\/ogarent\/content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_enable_lightbox_in_editor":"yes"}},"post":{"id":79,"title":"White Menu with Center Search","excerpt":""}};
</script>
<script type='text/javascript' src={{ asset('content/plugins/real1/assets/js/frontend.min91ac.js?ver=2.6.8')}}></script>
</body>
</html>
