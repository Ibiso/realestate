<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>Admin Dashboard</title>
</head>
<body>
    <div class="container">
        <section>
            @if(session()->has('error'))
                <div class="alert alert-danger">{{session()->get('error')}}</div>
            @endif

            @if(session()->has('success'))
                <div class="alert alert-success">{{session()->get('success')}}</div>
            @endif

            @include('layouts.admin.header')
            @include('layouts.admin.sidebar')
            <main>
                @yield('content')
            </main>
        </section>
       
    </div>
   

    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
</body>
</html>