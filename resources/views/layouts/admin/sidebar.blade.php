<aside id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link active" href="{{route('admin.dashboard')}}">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.listings')}}">Listings</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.create.listings')}}">Create Listings</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.listings-cat')}}"> Categories</a>
        </li>

       
       

        <li class="nav-item">
        <a class="nav-link " href="{{route('admin.logout')}}" tabindex="-1" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Logout</aside>
        </li>
    </ul>

    <form id="logout-form" method="POST" action="{{route('admin.logout')}}">@csrf</form>
</a>