@extends('layouts.real')

@section('content')
<section class="elementor-element elementor-element-7c8d elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="7c8d" data-element_type="section">
	<div class="elementor-container elementor-column-gap-no">
		<div class="elementor-row">
			<div class="elementor-element elementor-element-3924 elementor-column elementor-col-100 elementor-top-column" data-id="3924" data-element_type="column">
				<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-3db7 elementor-widget elementor-widget-header-search" data-id="3db7" data-element_type="widget" data-widget_type="header-search.default">
							<div class="elementor-widget-container">
								<h2 class="vis-hid">Headser elementor</h2>
								<section class="banner section-header-search " style="background-image:url('content/themes/ogarent/assets/images/banner/1.jpg')">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
												<div class="banner-content">
													<h2 class="">Discover best properties in one place</h2>
												</div>

												<form action="#" class="row banner-search sw_search_primary">

													<div class="form_field full banner_search_show">
														<div class="form-group field_search_where" style="">
															<input id="search_where" name="search_where" value="" type="text" class="form-control" placeholder="Enter Address, City or State" />
														</div>
													</div>

													<div class="form_field ">
														<div class="form-group">
															<div class="drop-menu">
																<div class="select">
																	<span>Any Purpose</span>
																	<i class="fa fa-angle-down"></i>
																</div>
																<input type="hidden" id="search_4" name="search_4" value="" />
																<ul class="dropeddown">
																	<li>Any Purpose</li>
																	<li data-value="For Sale">For Sale</li>
																	<li data-value="For Rent">For Rent</li>
																</ul>
															</div>
														</div>
													</div>
													<div class="form_field winter_dropdown_tree_style group_location_id search_field banner_search_show" style="">
														<div class="form-group">
															<script>
																var dp_fields_1 = {
																	"1": [""],
																	"2": [""],
																	"3": [""],
																	"4": [""],
																	"5": [""],
																	"6": [""],
																	"7": [""]
																}

																jQuery(document).ready(function($) {
																	$('#wintreeelem_0').winterTreefield({
																		ajax_url: 'https://listing-themes.com/ogarent-wp/wp-admin/admin-ajax.php',
																		ajax_param: {
																			"page": 'frontendajax_treefieldid',
																			"action": 'ci_action',
																			"table": 'treefield_m',
																			"field_id": '1',
																			"empty_value": 'All Categories'
																		},
																		attribute_id: 'idtreefield',
																		language_id: '1',
																		attribute_value: 'value',
																		skip_id: '',
																		empty_value: ' - ',
																		text_search: 'Search term',
																		text_no_results: 'No results found',
																		callback_selected: function(key) {
																			$('#wintreeelem_0').trigger("change");

																			$('*[class^="field_search_"]').show();

																			if (dp_fields_1[key]) {
																				$.each(dp_fields_1[key], function(key, value) {

																					// Hide all dependent fields
																					$('.field_search_' + value).hide();

																					$('.field_search_' + value).find('input:not([type="checkbox"])').val(null);
																					$('.field_search_' + value).find('input[type="checkbox"]').prop('checked', false);
																					$('.field_search_' + value).find('input:checked').removeAttr('checked');
																					$('.field_search_' + value).find($('option')).attr('selected', false)
																					//tinymce.execCommand( 'mceAddEditor', false, 'input_'+value+'_1' );

																				});
																			}
																		}
																	});
																});
															</script>
															<input name="search_category" value="" type="text" id="wintreeelem_0" readonly />
														</div><!-- /.form-group -->
													</div><!-- /.form-group -->
													<div class="form_field  banner_search_show side_hide  ">
														<div class="form-group">
															<div class="drop-menu">
																<div class="select">
																	<span>Min ($)</span>
																	<i class="fa fa-angle-down"></i>
																</div>
																<input type="hidden" id="search_36_from" name="search_36_from" value="" />
																<ul class="dropeddown">
																	<li>Min ($)</li>
																	<li data-value='10000'>$ 10000</li>
																	<li data-value='20000'>$ 20000</li>
																	<li data-value='30000'>$ 30000</li>
																	<li data-value='40000'>$ 40000</li>
																	<li data-value='50000'>$ 50000</li>
																	<li data-value='100000'>$ 100000</li>
																</ul>
															</div>
														</div>
													</div>
													<div class="form_field  banner_search_show  ">
														<div class="form-group">
															<div class="drop-menu">
																<div class="select">
																	<span>Max ($)</span>
																	<i class="fa fa-angle-down"></i>
																</div>
																<input type="hidden" id="search_36_to" name="search_36_to" value="" />
																<ul class="dropeddown">
																	<li>Max ($)</li>
																	<li data-value='10000'>$ 10000</li>
																	<li data-value='20000'>$ 20000</li>
																	<li data-value='30000'>$ 30000</li>
																	<li data-value='40000'>$ 40000</li>
																	<li data-value='50000'>$ 50000</li>
																	<li data-value='100000'>$ 100000</li>
																</ul>
															</div>
														</div>
													</div>
													<div class="form_field    ">
														<div class="form-group">
															<div class="drop-menu">
																<div class="select">
																	<span>Beds </span>
																	<i class="fa fa-angle-down"></i>
																</div>
																<input type="hidden" id="search_20" name="search_20" value="" />
																<ul class="dropeddown">
																	<li>Beds </li>
																	<li data-value='1'>1</li>
																	<li data-value='2'>2</li>
																	<li data-value='3'>3</li>
																	<li data-value='4'>4</li>
																	<li data-value='5'>5</li>
																	<li data-value='6'>6</li>
																</ul>
															</div>
														</div>
													</div>
													<div class="form_field winter_dropdown_tree_style group_location_id hide_on_all" style="">
														<div class="form-group">
															<script>
																var dp_fields_2 = []

																jQuery(document).ready(function($) {
																	$('#wintreeelem_1').winterTreefield({
																		ajax_url: 'https://listing-themes.com/ogarent-wp/wp-admin/admin-ajax.php',
																		ajax_param: {
																			"page": 'frontendajax_treefieldid',
																			"action": 'ci_action',
																			"table": 'treefield_m',
																			"field_id": '2',
																			"empty_value": 'All Locations'
																		},
																		attribute_id: 'idtreefield',
																		language_id: '1',
																		attribute_value: 'value',
																		skip_id: '',
																		empty_value: ' - ',
																		text_search: 'Search term',
																		text_no_results: 'No results found',
																		callback_selected: function(key) {
																			$('#wintreeelem_1').trigger("change");


																			if (dp_fields_2[key]) {
																				$.each(dp_fields_2[key], function(key, value) {

																					// Hide all dependent fields
																					$('.field_search_' + value).hide();

																					$('.field_search_' + value).find('input:not([type="checkbox"])').val(null);
																					$('.field_search_' + value).find('input[type="checkbox"]').prop('checked', false);
																					$('.field_search_' + value).find('input:checked').removeAttr('checked');
																					$('.field_search_' + value).find($('option')).attr('selected', false)
																					//tinymce.execCommand( 'mceAddEditor', false, 'input_'+value+'_1' );

																				});
																			}
																		}
																	});
																});
															</script>
															<input name="search_location" value="" type="text" id="wintreeelem_1" readonly />
														</div><!-- /.form-group -->
													</div><!-- /.form-group -->
													<div class="form_field srch-btn">
														<button id="search_header_button" type="submit" class="btn btn-outline-primary ">
															<i class="la la-search"></i>
															<span> Search</span>
															<i class="fa fa-spinner fa-spin fa-ajax-indicator" style="display: none;"></i>
														</button>
													</div>
												</form>


											</div>
										</div>
									</div>
								</section>




								<script>
									jQuery(document).ready(function($) {
										if (typeof $.fn.typeahead === 'function') {

											$('#location,#search_where').typeahead({
												minLength: 2,
												source: function(query, process) {
													var data = {
														q: query,
														limit: 8
													};

													$.extend(data, {
														'page': 'frontendajax_locationautocomplete',
														'action': 'ci_action',
														'template': ''
													});

													$.post('wp-admin/admin-ajax.html', data, function(data) {
														//console.log(data); // data contains array
														process(data);
													});
												}
											});
										}

										jQuery('#search_header_button').on('click', function() {
											jQuery(this).find(".fa-ajax-indicator").show();
											jQuery(this).find(".fa-ajax-hide").hide();
											//Define default data values for search
											var data = {};

											// Add custom data values, automatically by fields inside search-form
											jQuery('form.sw_search_primary input, form.sw_search_primary select, ' +
												'form.sw_search_secondary input, form.sw_search_secondary select').each(function(i) {

												if (jQuery(this).attr('type') == 'checkbox') {
													if (jQuery(this).attr('checked')) {
														data[jQuery(this).attr('name')] = jQuery(this).val();
													}
												} else if (jQuery(this).val() != '' && jQuery(this).val() != 0 && jQuery(this).val() != null) {
													data[jQuery(this).attr('name')] = jQuery(this).val();
												}

											});


											var gen_url = ogarent_generateUrl("results-page/index.html", data) + "#header-search";

											window.location = gen_url;

											return false;
										});


									});

									function ogarent_generateUrl(url, params) {
										var i = 0,
											key;
										for (key in params) {
											if (i === 0 && url.indexOf("?") === -1) {
												url += "?";
											} else {
												url += "&";
											}
											url += key;
											url += '=';
											url += params[key];
											i++;
										}
										return url;
									}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="elementor-element elementor-element-4390 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="4390" data-element_type="section">
	<div class="elementor-container elementor-column-gap-no">
		<div class="elementor-row">
			<div class="elementor-element elementor-element-7e53 elementor-column elementor-col-100 elementor-top-column" data-id="7e53" data-element_type="column">
				<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-2e42 elementor-widget elementor-widget-categories-presentation" data-id="2e42" data-element_type="widget" data-widget_type="categories-presentation.default">
							<div class="elementor-widget-container">

								<h2 class="vis-hid">Category elementor</h2>
								<section class="intro section-padding section-categories-presentation   ">
									<div class="container">
										<div class="row">
											<div class="col-xl-6 col-lg-6 pl-0">
												<div class="intro-content ">
													<h3 class="">Homes around the world</h3>
													<p class="">Are you looking for your prefered house ? Crest Rota Homes is the right place for you </p>
													<a href="#" class="btn btn-outline-primary view-btn">
														<i class="icon-arrow-right-circle"></i>View for rent</a>
												</div>
											</div>
											<div class="col-xl-6 col-lg-6 pr-0">
												<div class="intro-img">
													<img src="content/themes/ogarent/assets/images/intro/1.jpg" alt="" class="img-fluid">
												</div>
											</div>
										</div>
										<div class="intro-thumb-row">
											<a href="#" class="intro-thumb">
												<img src="content/themes/ogarent/assets/images/intro/thumb1.jpg" alt="">
												<h6 class="">Homes</h6>
											</a>
											<a href="#" class="intro-thumb">
												<img src="content/themes/ogarent/assets/images/intro/thumb2.jpg" alt="">
												<h6 class="">Appartments</h6>
											</a>
											<a href="#" class="intro-thumb">
												<img src="content/themes/ogarent/assets/images/intro/thumb3.jpg" alt="">
												<h6 class="">Garages</h6>
											</a>
										</div>
									</div>
								</section>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="elementor-element elementor-element-2caf elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="2caf" data-element_type="section">
	<div class="elementor-container elementor-column-gap-no">
		<div class="elementor-row">
			<div class="elementor-element elementor-element-6c8e elementor-column elementor-col-100 elementor-top-column" data-id="6c8e" data-element_type="column">
				<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-5489 elementor-widget elementor-widget-popular-listings" data-id="5489" data-element_type="widget" data-widget_type="popular-listings.default">
							<div class="elementor-widget-container">

								<h2 class="vis-hid">Listing elementor</h2>
								<section class="popular-listing section-padding  section-popular-listings   ">
									<div class="container">
										<div class="row justify-content-center">
											<div class="col-xl-6">
												<div class="section-heading">
													<span class="">Discover</span>

													<h3 class="">Popular Listing</h3>
												</div>
											</div>
										</div>
										<div class="row row-popu-listings">
											<div class="col-lg-4 col-md-6">
												<div class="card">
													<a href="listing-preview/princes-place-court/index.html" title="Princes Place Court">
														<div class="img-block">
															<div class="overlay"></div>
															<img src="content/uploads/sw_win/files/strict_cache/851x67828listing-28.jpg" alt="" class="img-fluid">
															<div class="rate-info">
																<h5>
																	$6700 </h5>
																<span class="purpose-for-rent">For Rent</span>
															</div>
														</div>
													</a>
													<div class="card-body">
														<a href="listing-preview/princes-place-court/index.html" title="Princes Place Court">
															<h3>Princes Place Court</h3>
															<p>
																<i class="la la-map-marker"></i>Wisconsin, Comfort Court 10 </p>
														</a>
														<div class="resul-items">
															<span class="property-card-value type_int  field_id_19" style=""><span class="field_name before hidden"> Bathrooms </span><span class="item" style=""> <span class="prefix"></span> 1 <span class="suffix"> </span> </span><span class="field_name after hidden"> Bathrooms </span></span><span class="property-card-value type_int  field_id_20" style=""><span class="field_name before hidden"> Beds </span><span class="item" style=""> <span class="prefix"></span> 3 <span class="suffix"> </span> </span><span class="field_name after hidden"> Beds </span></span><span class="property-card-value type_int  field_id_5" style=""><span class="field_name before hidden"> Area </span><span class="item" style=""> <span class="prefix"></span> 2202 <span class="suffix"> Ft&sup2; </span> </span><span class="field_name after hidden"> Area </span></span> </div>
													</div>
													<div class="card-footer">
														<span class="favorites-actions pull-left">
															<a href="#" data-id="28" class="add-favorites-action " data-loginpopup="true" data-ajax="wp-admin/admin-ajax9ed2.html?lang=en">
																<i class="la la-heart-o"></i>
															</a>
															<a href="#" data-id="28" class="remove-favorites-action hidden" data-ajax="wp-admin/admin-ajax9ed2.html?lang=en">
																<i class="la la-heart-o"></i>
															</a>
															<i class="fa fa-spinner fa-spin fa-custom-ajax-indicator"></i>
														</span>
														<a href="listing-preview/princes-place-court/index.html" title="2019-09-28 20:22:49" class="pull-right">
															<i class="la la-calendar-check-o"></i>
															2 days Ago </a>
													</div>
													<a href="listing-preview/princes-place-court/index.html" title="Princes Place Court" class="ext-link"></a>
												</div>
											</div>
											<div class="col-lg-4 col-md-6">
												<div class="card">
													<a href="listing-preview/lonsdale-place/index.html" title="Lonsdale Place">
														<div class="img-block">
															<div class="overlay"></div>
															<img src="content/uploads/sw_win/files/strict_cache/851x67825listing-25.jpg" alt="" class="img-fluid">
															<div class="rate-info">
																<h5>
																	$5500 </h5>
																<span class="purpose-for-sale">For Sale</span>
															</div>
														</div>
													</a>
													<div class="card-body">
														<a href="listing-preview/lonsdale-place/index.html" title="Lonsdale Place">
															<h3>Lonsdale Place</h3>
															<p>
																<i class="la la-map-marker"></i>Georgia, Pine Garden Lane 2 </p>
														</a>
														<div class="resul-items">
															<span class="property-card-value type_int  field_id_19" style=""><span class="field_name before hidden"> Bathrooms </span><span class="item" style=""> <span class="prefix"></span> 4 <span class="suffix"> </span> </span><span class="field_name after hidden"> Bathrooms </span></span><span class="property-card-value type_int  field_id_20" style=""><span class="field_name before hidden"> Beds </span><span class="item" style=""> <span class="prefix"></span> 3 <span class="suffix"> </span> </span><span class="field_name after hidden"> Beds </span></span><span class="property-card-value type_int  field_id_5" style=""><span class="field_name before hidden"> Area </span><span class="item" style=""> <span class="prefix"></span> 802 <span class="suffix"> Ft&sup2; </span> </span><span class="field_name after hidden"> Area </span></span> </div>
													</div>
													<div class="card-footer">
														<span class="favorites-actions pull-left">
															<a href="#" data-id="25" class="add-favorites-action " data-loginpopup="true" data-ajax="wp-admin/admin-ajax9ed2.html?lang=en">
																<i class="la la-heart-o"></i>
															</a>
															<a href="#" data-id="25" class="remove-favorites-action hidden" data-ajax="wp-admin/admin-ajax9ed2.html?lang=en">
																<i class="la la-heart-o"></i>
															</a>
															<i class="fa fa-spinner fa-spin fa-custom-ajax-indicator"></i>
														</span>
														<a href="listing-preview/lonsdale-place/index.html" title="2019-09-28 20:22:49" class="pull-right">
															<i class="la la-calendar-check-o"></i>
															2 days Ago </a>
													</div>
													<a href="listing-preview/lonsdale-place/index.html" title="Lonsdale Place" class="ext-link"></a>
												</div>
											</div>
											<div class="col-lg-4 col-md-6">
												<div class="card">
													<a href="listing-preview/amazing-apartment/index.html" title="Amazing Apartment">
														<div class="img-block">
															<div class="overlay"></div>
															<img src="content/uploads/sw_win/files/strict_cache/851x67822listing-22.jpg" alt="" class="img-fluid">
															<div class="rate-info">
																<h5>
																	$400 </h5>
																<span class="purpose-for-rent">For Rent</span>
															</div>
														</div>
													</a>
													<div class="card-body">
														<a href="listing-preview/amazing-apartment/index.html" title="Amazing Apartment">
															<h3>Amazing Apartment</h3>
															<p>
																<i class="la la-map-marker"></i>Louisiana, Norma Lane 10 </p>
														</a>
														<div class="resul-items">
															<span class="property-card-value type_int  field_id_19" style=""><span class="field_name before hidden"> Bathrooms </span><span class="item" style=""> <span class="prefix"></span> 4 <span class="suffix"> </span> </span><span class="field_name after hidden"> Bathrooms </span></span><span class="property-card-value type_int  field_id_20" style=""><span class="field_name before hidden"> Beds </span><span class="item" style=""> <span class="prefix"></span> 1 <span class="suffix"> </span> </span><span class="field_name after hidden"> Beds </span></span><span class="property-card-value type_int  field_id_5" style=""><span class="field_name before hidden"> Area </span><span class="item" style=""> <span class="prefix"></span> 2502 <span class="suffix"> Ft&sup2; </span> </span><span class="field_name after hidden"> Area </span></span> </div>
													</div>
													<div class="card-footer">
														<span class="favorites-actions pull-left">
															<a href="#" data-id="22" class="add-favorites-action " data-loginpopup="true" data-ajax="wp-admin/admin-ajax9ed2.html?lang=en">
																<i class="la la-heart-o"></i>
															</a>
															<a href="#" data-id="22" class="remove-favorites-action hidden" data-ajax="wp-admin/admin-ajax9ed2.html?lang=en">
																<i class="la la-heart-o"></i>
															</a>
															<i class="fa fa-spinner fa-spin fa-custom-ajax-indicator"></i>
														</span>
														<a href="listing-preview/amazing-apartment/index.html" title="2019-09-28 20:22:49" class="pull-right">
															<i class="la la-calendar-check-o"></i>
															2 days Ago </a>
													</div>
													<a href="listing-preview/amazing-apartment/index.html" title="Amazing Apartment" class="ext-link"></a>
												</div>
											</div>

										</div>
									</div>

								</section>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="elementor-element elementor-element-439a elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="439a" data-element_type="section">
	<div class="elementor-container elementor-column-gap-no">
		<div class="elementor-row">
			<div class="elementor-element elementor-element-3045 elementor-column elementor-col-100 elementor-top-column" data-id="3045" data-element_type="column">
				<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-8bc elementor-widget elementor-widget-services-you-need" data-id="8bc" data-element_type="widget" data-widget_type="services-you-need.default">
							<div class="elementor-widget-container">

								<h2 class="vis-hid">Serices elementor</h2>
								<section class="explore-feature hp_s1 section-padding section-services-you-need  ">
									<div class="container">
										<div class="row justify-content-center">
											<div class="col-xl-6">
												<div class="section-heading">
													<span class="">Explore Features</span>

													<h3 class="">Service You Need</h3>
												</div>
											</div>
										</div>
										<div class="row">


											<div class="col-xl-3 col-sm-6 col-md-6 col-lg-4">
												<a href="#" title="">
													<div class="card">
														<div class="card-body">
															<i class="la la-home"></i>
															<h3 class="">Crest rota Villa</h3>
															<p class="">We’re creating a seamless online experience – from shopping on the largest rental network, to applying, to paying rent. Find rentals </p>
														</div>
													</div>
													<!--card end-->
												</a>
											</div>

											<div class="col-xl-3 col-sm-6 col-md-6 col-lg-4">
												<a href="#" title="">
													<div class="card">
														<div class="card-body">
															<i class="la la-hand-pointer-o"></i>
															<h3 class="">Crest Rota Tech</h3>
															<p class="">We provide clean and reliable power for productive use and also top notch maintainance.</p>
														</div>
													</div>
													<!--card end-->
												</a>
											</div>

											<div class="col-xl-3 col-sm-6 col-md-6 col-lg-4">
												<a href="#" title="">
													<div class="card">
														<div class="card-body">
															<i class="la la-unlock"></i>
															<h3 class="">Crest Rota Mobile</h3>
															<p class="">Do you have an event, occasion or you feel like having a tour and you need a good car, Crest Rota is here for you</p>
														</div>
													</div>
													<!--card end-->
												</a>
											</div>

											<div class="col-xl-3 col-sm-6 col-md-6 col-lg-4">
												<a href="#" title="">
													<div class="card">
														<div class="card-body">
															<i class="la la-star-o"></i>
															<h3 class="">Interior Decoration</h3>
															<p class="">Every property owner needs beautiful place to call his own crest rota offers interior decoration of homes and event places</p>
														</div>
													</div>
													<!--card end-->
												</a>
											</div>

										</div>
									</div>
								</section>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="elementor-element elementor-element-bc1 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="bc1" data-element_type="section">
	<div class="elementor-container elementor-column-gap-no">
		<div class="elementor-row">
			<div class="elementor-element elementor-element-74a8 elementor-column elementor-col-100 elementor-top-column" data-id="74a8" data-element_type="column">
				<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-64cb elementor-widget elementor-widget-popular-cities" data-id="64cb" data-element_type="widget" data-widget_type="popular-cities.default">
							<div class="elementor-widget-container">
								<h2 class="vis-hid">Cities elementor</h2>

								<section class="popular-cities hp_s1 section-padding section-popular-cities  ">
									<div class="container">
										<div class="row justify-content-center">
											<div class="col-xl-6">
												<div class="section-heading">
													<span class="">Popular Cities</span>

													<h3 class="">Find Perfect Place</h3>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4 col-md-6">
												<a href="results-page/index2e3d.html?search_location=24">
													<div class="card">
														<div class="overlay"></div>
														<img src="content/uploads/2019/09/location_01-570x570.jpg" alt="New York" class="img-fluid">
														<div class="card-body">
															<h4>New York</h4>
															<p>
																4 Listing </p>
															<i class="fa fa-angle-right"></i>
														</div>
													</div>
												</a>
											</div>
											<div class="col-lg-4 col-md-6">
												<a href="results-page/index870e.html?search_location=25">
													<div class="card">
														<div class="overlay"></div>
														<img src="content/uploads/2019/09/location_02-570x570.jpg" alt="Florida" class="img-fluid">
														<div class="card-body">
															<h4>Florida</h4>
															<p>
																5 Listing </p>
															<i class="fa fa-angle-right"></i>
														</div>
													</div>
												</a>
											</div>
											<div class="col-lg-4 col-md-6">
												<a href="results-page/index2d13.html?search_location=26">
													<div class="card">
														<div class="overlay"></div>
														<img src="content/uploads/2019/09/location_03-570x570.jpg" alt="Massachusetts" class="img-fluid">
														<div class="card-body">
															<h4>Massachusetts</h4>
															<p>
																3 Listing </p>
															<i class="fa fa-angle-right"></i>
														</div>
													</div>
												</a>
											</div>
										</div>
									</div>
								</section>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="elementor-element elementor-element-721ds elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="721ds" data-element_type="section">
	<div class="elementor-container elementor-column-gap-no">
		<div class="elementor-row">
			<div class="elementor-element elementor-element-8c51 elementor-column elementor-col-100 elementor-top-column" data-id="8c51" data-element_type="column">
				<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-7335c elementor-widget elementor-widget-clients-say" data-id="7335c" data-element_type="widget" data-widget_type="clients-say.default">
							<div class="elementor-widget-container">
								<h2 class="vis-hid">Clients say elementor</h2>
								<section class="testimonial-sec section-padding hp2 section-clients-say  ">
									<div class="container">
										<div class="row justify-content-center">
											<div class="col-xl-6">
												<div class="section-heading">
													<span class="">Clients Say</span>

													<h3 class="">Testimonials</h3>
												</div>
											</div>
										</div>
										<div class="row testimonail-sect">
											<div class="comment-carousel">

												<div class="comment-info">
													<p class="">Aenean sollicitudin, lorem quis bibendum auctor, nisi elitco nsequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accum sanpsum velit. Nam nec tellus a odio tinci.</p>
													<div class="cm-info-sec">
														<div class="cm-img">
															<img src="content/themes/ogarent/assets/images/resources/cm-img2.png" alt="">
														</div>
														<!--author-img end-->
														<div class="cm-info">
															<h3>Kritsofer Nolan</h3>
															<h4>Property Owner</h4>
														</div>
													</div>
													<!--cm-info-sec end-->
												</div>
												<!--comment-info end-->

												<div class="comment-info">
													<p class="">Aenean sollicitudin, lorem quis bibendum auctor, nisi elitco nsequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accum sanpsum velit. Nam nec tellus a odio tinci.</p>
													<div class="cm-info-sec">
														<div class="cm-img">
															<img src="content/themes/ogarent/assets/images/resources/cm-img3.png" alt="">
														</div>
														<!--author-img end-->
														<div class="cm-info">
															<h3>Kritsofer Nolan</h3>
															<h4>Property Owner</h4>
														</div>
													</div>
													<!--cm-info-sec end-->
												</div>
												<!--comment-info end-->

												<div class="comment-info">
													<p class="">Aenean sollicitudin, lorem quis bibendum auctor, nisi elitco nsequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accum sanpsum velit. Nam nec tellus a odio tinci.</p>
													<div class="cm-info-sec">
														<div class="cm-img">
															<img src="content/themes/ogarent/assets/images/resources/cm-img2.png" alt="">
														</div>
														<!--author-img end-->
														<div class="cm-info">
															<h3>Kritsofer Nolan</h3>
															<h4>Property Owner</h4>
														</div>
													</div>
													<!--cm-info-sec end-->
												</div>
												<!--comment-info end-->

												<div class="comment-info">
													<p class="">Aenean sollicitudin, lorem quis bibendum auctor, nisi elitco nsequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accum sanpsum velit. Nam nec tellus a odio tinci.</p>
													<div class="cm-info-sec">
														<div class="cm-img">
															<img src="content/themes/ogarent/assets/images/resources/cm-img3.png" alt="">
														</div>
														<!--author-img end-->
														<div class="cm-info">
															<h3>Kritsofer Nolan</h3>
															<h4>Property Owner</h4>
														</div>
													</div>
													<!--cm-info-sec end-->
												</div>
												<!--comment-info end-->

												<div class="comment-info">
													<p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elitco nsequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accum sanpsum velit. Nam nec tellus a odio tinci.</p>
													<div class="cm-info-sec">
														<div class="cm-img">
															<img src="content/themes/ogarent/assets/images/resources/cm-img2.png" alt="">
														</div>
														<!--author-img end-->
														<div class="cm-info">
															<h3>Kritsofer Nolan</h3>
															<h4>Property Owner</h4>
														</div>
													</div>
													<!--cm-info-sec end-->
												</div>
												<!--comment-info end-->

												<div class="comment-info">
													<p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elitco nsequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accum sanpsum velit. Nam nec tellus a odio tinci.</p>
													<div class="cm-info-sec">
														<div class="cm-img">
															<img src="content/themes/ogarent/assets/images/resources/cm-img3.png" alt="">
														</div>
														<!--author-img end-->
														<div class="cm-info">
															<h3>Kritsofer Nolan</h3>
															<h4>Property Owner</h4>
														</div>
													</div>
													<!--cm-info-sec end-->
												</div>
												<!--comment-info end-->

											</div>
											<!--comment-carousel end-->
										</div>
										<!--testimonail-sect end-->

										<br style="clear:both;" />
									</div>
								</section>






							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3975.5879628753523!2d7.065779914263339!3d4.84058824177774!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1069cd6bc340f4e3%3A0xf2e89b269a77fbb6!2sDr%20Ehoro%20Cl%2C%20Elelenwo%2C%20Port%20Harcourt!5e0!3m2!1sen!2sng!4v1573245912447!5m2!1sen!2sng" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
<a href="#" class="section-slogan" title="">
	<section class="cta st2 section-padding" style="background-color:#303e94">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="cta-text">
						<h2 class="">Discover a home you'll love to stay</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
</a>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


@endsection