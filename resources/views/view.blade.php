@extends('layouts.real')

@section('content')
<section class="elementor-element elementor-element-3e2c elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="3e2c" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-11df elementor-column elementor-col-100 elementor-top-column" data-id="11df" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-687e elementor-widget elementor-widget-header-title" data-id="687e" data-element_type="widget" data-widget_type="header-title.default">
                            <div class="elementor-widget-container">
                                <h2 class="vis-hid">Header elementor</h2>

                                <section class="pager-sec bfr " style="background-image: url(../wp-content/themes/selio/assets/images/resources/pager-bg.jpg);">
                                    <div class="container">
                                        <div class="pager-sec-details">
                                            <h3 class="">List Layout</h3>
                                            <ul>
                                                <li class="item">
                                                    <a href="../index.html">Home</a>
                                                </li>
                                                <li>
                                                    <span>Listing</span>
                                                </li>
                                                <li>
                                                    <span>List Layout with Sidebar</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--pager-sec-details end-->
                                    </div>
                                </section>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="elementor-element elementor-element-4771 elementor-section-stretched blog-wrapper elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="4771" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
    <div class="elementor-container elementor-column-gap-narrow">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-e8a elementor-column elementor-col-66 elementor-top-column" data-id="e8a" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-10f5 elementor-widget elementor-widget-result-listings" data-id="10f5" data-element_type="widget" data-widget_type="result-listings.default">
                            <div class="elementor-widget-container">
                                <div class="selio_sw_win_wrapper">
                                    <div class="ci sw_widget sw_wrap">
                                        <div id="results" class="properties-rows">
                                            <span id="results_top"></span>

                                            <div class="list-head">
                                                <div class="sortby">
                                                    <span>Sort by:</span>
                                                    <div class="drop-menu">
                                                        <div class="select">
                                                            <span>Newest</span>
                                                            <i class="la la-caret-down"></i>
                                                        </div>
                                                        <input type="hidden" name="search_order" id="search_order">
                                                        <ul class="dropeddown">
                                                            <li data-value="idlisting DESC">Newest</li>
                                                            <li data-value="idlisting ASC">Oldest</li>
                                                            <li data-value="counter_views DESC, idlisting DESC">Most Viewed</li>
                                                            <li data-value="field_36_int DESC, idlisting DESC">Higher price</li>
                                                            <li data-value="field_36_int ASC, idlisting DESC">Lower price</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!--sortby end-->
                                                <div class="view-change">
                                                    <ul class="nav nav-tabs sw-order-view">
                                                        <li class="nav-item">
                                                            <a href="#" class="nav-link grid view-type active" data-ref="grid"><i class="la la-th-large"></i></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="#" class="nav-link list view-type " data-ref="list"><i class="la la-th-list"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!--view-change end-->
                                            </div>
                                            <!--list-head end-->


                                            <div class="listings">
                                                <div class="list_products">
                                                    <div class="row">
                                                       
                                                       
                                                        
                                                        @forelse($listings as $listing)
                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="card">
                                                                <a href="{{route('front.view.listing',['id'=>$listing->id])}}" title="{{$listing->name}}">
                                                                    <div class="img-block">
                                                                        <div class="overlay"></div>
                                                                        <img src="{{asset('storage/listings/'.$listing->photo)}}" alt="{{$listing->name}}" class="img-fluid">
                                                                        <div class="rate-info">
                                                                            <h5>
                                                                                @if($listing->status == 1)
                                                                                <span class="purpose-for-rent">For Rent</span>
                                                                                @else
                                                                                <span class="purpose-for-sale">For Sale</span>
                                                                                @endif
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                                <div class="card-body">
                                                                    <a href="{{route('front.view.listing',['id'=>$listing->id])}}" title="{{$listing->name}}">
                                                                        <h3>{{$listing->name}}</h3>
                                                                        <p>
                                                                            <i class="la la-map-marker"></i>{{$listing->location}}</p>
                                                                    </a>
                                                                    <div class="resul-items">
                                                                        <span class="property-card-value type_int  field_id_19" style=""><span class="field_name before hidden"> Bathrooms </span><span class="item" style=""> <span class="prefix"></span> {{$listing->bathroom}} <span class="suffix"> </span> </span><span class="field_name after hidden"> Bathrooms </span></span><span class="property-card-value type_int  field_id_20" style=""><span class="field_name before hidden"> Beds </span><span class="item" style=""> <span class="prefix"></span> {{$listing->bedroom}} <span class="suffix"> </span> </span><span class="field_name after hidden"> Beds </span></span><span class="property-card-value type_int  field_id_5" style=""><span class="field_name before hidden"> Area </span><span class="item" style=""> <span class="prefix"></span> {{$listing->area}} <span class="suffix"> Ft&sup2; </span> </span><span class="field_name after hidden"> Area </span></span> </div>
                                                                </div>
                                                                <div class="card-footer">
                                                                    <span class="favorites-actions pull-left">
                                                                        <a href="#" data-id="29" class="add-favorites-action " data-loginpopup="true" data-ajax="../wp-admin/admin-ajax9ed2.html?lang=en">
                                                                            <i class="la la-heart-o"></i>
                                                                        </a>
                                                                        <a href="#" data-id="29" class="remove-favorites-action hidden" data-ajax="../wp-admin/admin-ajax9ed2.html?lang=en">
                                                                            <i class="la la-heart-o"></i>
                                                                        </a>
                                                                        <i class="fa fa-spinner fa-spin fa-custom-ajax-indicator"></i>
                                                                    </span>
                                                                    <a href="{{route('front.view.listing',['id'=>$listing->id])}}" title="2019-09-28 20:23:13" class="pull-right">
                                                                        <i class="la la-calendar-check-o"></i>
                                                                        {{$listing->created_at}} </a>
                                                                </div>
                                                                <a href="{{route('front.view.listing',['id'=>$listing->id])}}" title="{{$listing->name}}" class="ext-link"></a>
                                                            </div>
                                                        </div>
                                                        @empty
                                                            <p> We currently do not have any listings yet</p>
                                                        @endforelse
                                                    </div>
                                                </div>

                                            </div>
                                            <!--tab-content end-->
                                            {{$listings->links()}}
                                           
                                        </div>

                                    </div>
                                </div>
                                <h2 class="vis-hid">Results elementor</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-1118 elementor-column elementor-col-33 elementor-top-column" data-id="1118" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-6cfb elementor-widget elementor-widget-sidebar" data-id="6cfb" data-element_type="widget" data-widget_type="sidebar.default">
                            <div class="elementor-widget-container">
                                <div id="categories-4" class="widget widget_categories side">
                                    <h3 class="widget-title">Categories</h3>
                                    <ul>
                                        @foreach($categories as $cat )
                                            <li class="cat-item cat-item-5"><a href="#">{{$cat->name}}</a> <span class="post_count"> {{$cat->listings->count()}} </span>
                                            </li>
                                        @endforeach
                                       
                                    </ul>
                                </div> <!-- end widget -->
                                <div id="sw_win_primarysearch_widget-1" class="widget widget_sw_win_primarysearch_widget side">
                                    <h3 class="widget-title">Primary search</h3>
                                    <div class="selio_sw_win_wrapper">
                                        <div class="ci sw_widget sw_wrap">
                                            <div class="widget-property-search">
                                                <form action="#" class="row sw_search_primary sw_search_form banner-search clearfix">

                                                    <div class="form_field full banner_search_show">
                                                        <div class="form-group field_search_where" style="">
                                                            <input id="search_where" name="search_where" value="" type="text" class="form-control" placeholder="Enter Address, City or State" />
                                                        </div>
                                                    </div>

                                                    <div class="form_field ">
                                                        <div class="form-group">
                                                            <div class="drop-menu">
                                                                <div class="select">
                                                                    <span>Any Purpose</span>
                                                                    <i class="fa fa-angle-down"></i>
                                                                </div>
                                                                <input type="hidden" id="search_4" name="search_4" value="" />
                                                                <ul class="dropeddown">
                                                                    <li>Any Purpose</li>
                                                                    <li data-value="For Sale">For Sale</li>
                                                                    <li data-value="For Rent">For Rent</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form_field winter_dropdown_tree_style group_location_id search_field banner_search_show" style="">
                                                        <div class="form-group">
                                                            <input name="search_category" value="" type="text" id="wintreeelem_0" readonly /> </div><!-- /.form-group -->
                                                    </div><!-- /.form-group -->
                                                    <div class="form_field banner_search_show side_hide">
                                                        <div class="form-group">
                                                            <div class="drop-menu">
                                                                <div class="select">
                                                                    <span>Min ($)</span>
                                                                    <i class="fa fa-angle-down"></i>
                                                                </div>
                                                                <input type="hidden" id="search_36_from" name="search_36_from" value="" />
                                                                <ul class="dropeddown">
                                                                    <li>Min ($)</li>
                                                                    <li data-value="$ 10000">$ 10000</li>
                                                                    <li data-value="$ 20000">$ 20000</li>
                                                                    <li data-value="$ 30000">$ 30000</li>
                                                                    <li data-value="$ 40000">$ 40000</li>
                                                                    <li data-value="$ 50000">$ 50000</li>
                                                                    <li data-value="$ 100000">$ 100000</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_field banner_search_show">
                                                        <div class="form-group">
                                                            <div class="drop-menu">
                                                                <div class="select">
                                                                    <span>Max ($)</span>
                                                                    <i class="fa fa-angle-down"></i>
                                                                </div>
                                                                <input type="hidden" id="search_36_to" name="search_36_to" value="" />
                                                                <ul class="dropeddown">
                                                                    <li>Max ($)</li>
                                                                    <li data-value="$ 10000">$ 10000</li>
                                                                    <li data-value="$ 20000">$ 20000</li>
                                                                    <li data-value="$ 30000">$ 30000</li>
                                                                    <li data-value="$ 40000">$ 40000</li>
                                                                    <li data-value="$ 50000">$ 50000</li>
                                                                    <li data-value="$ 100000">$ 100000</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_field ">
                                                        <div class="form-group">
                                                            <div class="drop-menu">
                                                                <div class="select">
                                                                    <span>Beds </span>
                                                                    <i class="fa fa-angle-down"></i>
                                                                </div>
                                                                <input type="hidden" id="search_20" name="search_20" value="" />
                                                                <ul class="dropeddown">
                                                                    <li>Beds </li>
                                                                    <li data-value="1">1</li>
                                                                    <li data-value="2">2</li>
                                                                    <li data-value="3">3</li>
                                                                    <li data-value="4">4</li>
                                                                    <li data-value="5">5</li>
                                                                    <li data-value="6">6</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_field winter_dropdown_tree_style form-group group_location_id hide_on_all" style="">
                                                        <div class="form-group">
                                                            <input name="search_location" value="" type="text" id="wintreeelem_1" readonly /> </div><!-- /.form-group -->
                                                    </div><!-- /.form-group -->
                                                    <div class="form_field">
                                                        <button class="btn btn-outline-primary sw-search-start" type='submit'>
                                                            <span>Search<i class="fa fa-spinner fa-spin fa-ajax-indicator" style="display: none;"></i></span>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>


                                        </div>
                                    </div>
                                </div> <!-- end widget -->
                                <div id="sw_win_secondarysearch_widget-1" class="widget widget_sw_win_secondarysearch_widget side">
                                    <h3 class="widget-title">Secondary search</h3>
                                    <div class="selio_sw_win_wrapper">
                                        <div class="ci sw_widget sw_wrap">

                                            <div class="widget-property-search">
                                                <form action="#" class="row sw_search_primary sw_search_form banner-search clearfix">
                                                    <div class="input-field field_search_22 " style="-">
                                                        <input type="checkbox" name="search_22" id="search_22" rel="Air conditioning" value="1">
                                                        <label for="search_22">
                                                            <span></span>
                                                            <small>Air conditioning</small>
                                                        </label>
                                                    </div>
                                                    <div class="input-field field_search_29 " style="-">
                                                        <input type="checkbox" name="search_29" id="search_29" rel="Internet" value="1">
                                                        <label for="search_29">
                                                            <span></span>
                                                            <small>Internet</small>
                                                        </label>
                                                    </div>
                                                    <div class="input-field field_search_32 " style="-">
                                                        <input type="checkbox" name="search_32" id="search_32" rel="Parking" value="1">
                                                        <label for="search_32">
                                                            <span></span>
                                                            <small>Parking</small>
                                                        </label>
                                                    </div>
                                                    <div class="input-field field_search_11 " style="-">
                                                        <input type="checkbox" name="search_11" id="search_11" rel="Balcony" value="1">
                                                        <label for="search_11">
                                                            <span></span>
                                                            <small>Balcony</small>
                                                        </label>
                                                    </div>
                                                    <div class="input-field field_search_33 " style="-">
                                                        <input type="checkbox" name="search_33" id="search_33" rel="Pool" value="1">
                                                        <label for="search_33">
                                                            <span></span>
                                                            <small>Pool</small>
                                                        </label>
                                                    </div>
                                                    <div class="input-field field_search_31 " style="-">
                                                        <input type="checkbox" name="search_31" id="search_31" rel="Microwave" value="1">
                                                        <label for="search_31">
                                                            <span></span>
                                                            <small>Microwave</small>
                                                        </label>
                                                    </div>
                                                    <div class="input-field field_search_23 " style="-">
                                                        <input type="checkbox" name="search_23" id="search_23" rel="Cable TV" value="1">
                                                        <label for="search_23">
                                                            <span></span>
                                                            <small>Cable TV</small>
                                                        </label>
                                                    </div>
                                                    <div class="form_field">
                                                        <button class="btn btn-outline-primary sw-search-start" type='submit'>
                                                            <span>Search<i class="fa fa-spinner fa-spin fa-ajax-indicator" style="display: none;"></i></span>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end widget -->
                                <div id="sw_win_featuredlisting_widget-1" class="widget widget_sw_win_featuredlisting_widget side">
                                    <h3 class="widget-title">Featured listings</h3>
                                    <div class="selio_sw_win_wrapper">
                                        <div class="ci sw_widget sw_wrap">

                                            <div class="result-container-latest-side">


                                                <div class="card">
                                                    <a href="../listing-preview/palace-square/index.html" title="Palace Square">
                                                        <div class="img-block">
                                                            <div class="overlay"></div>
                                                            <img src="../wp-content/uploads/sw_win/files/strict_cache/851x67829listing-29.jpg" alt="Palace Square" class="img-fluid">
                                                            <div class="rate-info">
                                                                <h5>
                                                                    $5500 </h5>
                                                                <span class="purpose-for-rent">For Rent</span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="card-body">
                                                        <a href="../listing-preview/palace-square/index.html" title="Palace Square">
                                                            <h3>Palace Square</h3>
                                                            <p>
                                                                <i class="la la-map-marker"></i>New York, Pen Road 1 </p>
                                                        </a>
                                                        <div class="resul-items d-none">
                                                            <span class="property-card-value type_int  field_id_19" style=""><span class="field_name before hidden"> Bathrooms </span><span class="item" style=""> <span class="prefix"></span> 2 <span class="suffix"> </span> </span><span class="field_name after hidden"> Bathrooms </span></span><span class="property-card-value type_int  field_id_20" style=""><span class="field_name before hidden"> Beds </span><span class="item" style=""> <span class="prefix"></span> 1 <span class="suffix"> </span> </span><span class="field_name after hidden"> Beds </span></span><span class="property-card-value type_int  field_id_5" style=""><span class="field_name before hidden"> Area </span><span class="item" style=""> <span class="prefix"></span> 3002 <span class="suffix"> Ft&sup2; </span> </span><span class="field_name after hidden"> Area </span></span> </div>
                                                    </div>
                                                    <div class="card-footer d-none">
                                                        <span class="favorites-actions pull-left">
                                                            <a href="#" data-id="29" class="add-favorites-action " data-loginpopup="true" data-ajax="../wp-admin/admin-ajax9ed2.html?lang=en">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" data-id="29" class="remove-favorites-action hidden" data-ajax="../wp-admin/admin-ajax9ed2.html?lang=en">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <i class="fa fa-spinner fa-spin fa-custom-ajax-indicator"></i>
                                                        </span>
                                                        <a href="../listing-preview/palace-square/index.html" title="2019-09-28 20:23:13" class="pull-right">
                                                            <i class="la la-calendar-check-o"></i>
                                                            2 days Ago </a>
                                                    </div>
                                                    <a href="../listing-preview/palace-square/index.html" title="Palace Square" class="ext-link"></a>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div> <!-- end widget -->
                                <div id="categories-6" class="widget widget_categories side">
                                    <h3 class="widget-title">Categories</h3>
                                    <ul>
                                        <li class="cat-item cat-item-5"><a href="../category/business/index.html">Business</a> <span class="post_count"> 1 </span>
                                        </li>
                                        <li class="cat-item cat-item-4"><a href="../category/lifestyle/index.html">Lifestyle</a> <span class="post_count"> 4 </span>
                                        </li>
                                        <li class="cat-item cat-item-2"><a href="../category/models/index.html">Models</a> <span class="post_count"> 3 </span>
                                        </li>
                                        <li class="cat-item cat-item-6"><a href="../category/travel/index.html">Travel</a> <span class="post_count"> 3 </span>
                                        </li>
                                    </ul>
                                </div> <!-- end widget -->
                                <div id="sw_win_latestlisting_widget-3" class="widget widget_sw_win_latestlisting_widget side">
                                    <h3 class="widget-title">Latest listings</h3>
                                    <div class="selio_sw_win_wrapper">
                                        <div class="ci sw_widget sw_wrap">
                                            <div class="widget-posts">

                                                <ul>
                                                    <li>
                                                        <div class="wd-posts">
                                                            <div class="ps-img">
                                                                <a href="../listing-preview/palace-square/index.html" title="Palace Square">
                                                                    <img src="../wp-content/uploads/sw_win/files/strict_cache/224x17829listing-29.jpg" alt="Palace Square">
                                                                </a>
                                                            </div>
                                                            <!--ps-img end-->
                                                            <div class="ps-info">
                                                                <h3><a href="../listing-preview/palace-square/index.html" title="Palace Square">Palace Square</a></h3>
                                                                <strong>
                                                                    $5500 </strong>
                                                                <span><i class="la la-map-marker"></i>New York, Pen Road 1</span>
                                                            </div>
                                                            <!--ps-info end-->
                                                        </div>
                                                        <!--wd-posts end-->
                                                    </li>
                                                    <li>
                                                        <div class="wd-posts">
                                                            <div class="ps-img">
                                                                <a href="../listing-preview/princes-place-court/index.html" title="Princes Place Court">
                                                                    <img src="../wp-content/uploads/sw_win/files/strict_cache/224x17828listing-28.jpg" alt="Princes Place Court">
                                                                </a>
                                                            </div>
                                                            <!--ps-img end-->
                                                            <div class="ps-info">
                                                                <h3><a href="../listing-preview/princes-place-court/index.html" title="Princes Place Court">Princes Place Court</a></h3>
                                                                <strong>
                                                                    $6700 </strong>
                                                                <span><i class="la la-map-marker"></i>Wisconsin, Comfort Court 10</span>
                                                            </div>
                                                            <!--ps-info end-->
                                                        </div>
                                                        <!--wd-posts end-->
                                                    </li>
                                                    <li>
                                                        <div class="wd-posts">
                                                            <div class="ps-img">
                                                                <a href="../listing-preview/atrium-house/index.html" title="Atrium House">
                                                                    <img src="../wp-content/uploads/sw_win/files/strict_cache/224x17827listing-27.jpg" alt="Atrium House">
                                                                </a>
                                                            </div>
                                                            <!--ps-img end-->
                                                            <div class="ps-info">
                                                                <h3><a href="../listing-preview/atrium-house/index.html" title="Atrium House">Atrium House</a></h3>
                                                                <strong>
                                                                    $8100 </strong>
                                                                <span><i class="la la-map-marker"></i>California, Renzelli Boulevard 7</span>
                                                            </div>
                                                            <!--ps-info end-->
                                                        </div>
                                                        <!--wd-posts end-->
                                                    </li>
                                                    <li>
                                                        <div class="wd-posts">
                                                            <div class="ps-img">
                                                                <a href="../listing-preview/elysian-apartments/index.html" title="Elysian Apartments">
                                                                    <img src="../wp-content/uploads/sw_win/files/strict_cache/224x17826listing-26.jpg" alt="Elysian Apartments">
                                                                </a>
                                                            </div>
                                                            <!--ps-img end-->
                                                            <div class="ps-info">
                                                                <h3><a href="../listing-preview/elysian-apartments/index.html" title="Elysian Apartments">Elysian Apartments</a></h3>
                                                                <strong>
                                                                    $2600 </strong>
                                                                <span><i class="la la-map-marker"></i>Indiana, Bernardo Street 9</span>
                                                            </div>
                                                            <!--ps-info end-->
                                                        </div>
                                                        <!--wd-posts end-->
                                                    </li>
                                                    <li>
                                                        <div class="wd-posts">
                                                            <div class="ps-img">
                                                                <a href="../listing-preview/lonsdale-place/index.html" title="Lonsdale Place">
                                                                    <img src="../wp-content/uploads/sw_win/files/strict_cache/224x17825listing-25.jpg" alt="Lonsdale Place">
                                                                </a>
                                                            </div>
                                                            <!--ps-img end-->
                                                            <div class="ps-info">
                                                                <h3><a href="../listing-preview/lonsdale-place/index.html" title="Lonsdale Place">Lonsdale Place</a></h3>
                                                                <strong>
                                                                    $5500 </strong>
                                                                <span><i class="la la-map-marker"></i>Georgia, Pine Garden Lane 2</span>
                                                            </div>
                                                            <!--ps-info end-->
                                                        </div>
                                                        <!--wd-posts end-->
                                                    </li>
                                                </ul>


                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end widget -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="elementor-element elementor-element-26c4 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="26c4" data-element_type="section">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-5dd7 elementor-column elementor-col-100 elementor-top-column" data-id="5dd7" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-956 elementor-widget elementor-widget-slogan" data-id="956" data-element_type="widget" data-widget_type="slogan.default">
                            <div class="elementor-widget-container">
                                <h2 class="vis-hid">Slogan elementor</h2>

                                <a href="#" class="section-slogan" title="">
                                    <section class="cta st2 section-padding" style="background-color:#303e94">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="cta-text">
                                                        <h2 class="">Discover a home you'll love to stay</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </a>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection