@extends('layouts.admin.app')
@section('content')

    <table class="table table-responsive table-bordered">
        <thead>
            <th>Name</th>
            <th>Price</th>
            <th>Location</th>
            <th>Bathroom</th>
            <th>Bedroom</th>
            <th>Area</th>
            <th>Description</th>
            <th>Cooling</th>
            <th>Status</th>
            <th>Action</th>
            <th>Delete</th>
        </thead>

        <tbody>
            @forelse($listings as $listing)
                <tr>
                    <td>{{$listing->name}}</td>
                    <td>{{number_format($listing->price,2)}}</td>
                    <td>{{$listing->location}}</td>
                    <td>{{$listing->bathroom}}</td>
                    <td>{{$listing->bedroom}}</td>
                    <td>{{$listing->area}}</td>
                   
                    <td>{{$listing->description}}</td>
                    <td>{{$listing->cooling}}</td>
                    <td>{{$listing->status == 1 ? 'For Rent' :'For Sale'}}</td>
                    <td><a href="{{route('admin.edit.listings',['id'=>$listing->id])}}" class="badge badge-primary">View</a></td>
                    <td>
                        <form method="POST" action="{{route('admin.destroy.listings',['id'=>$listing->id])}}">
                            @csrf
                            <input type="submit" name="submit" class="btn btn-danger" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <p> There is no property listing yet</p>
            @endforelse
        </tbody>

    </table>
@endsection