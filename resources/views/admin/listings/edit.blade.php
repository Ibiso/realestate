@extends('layouts.admin.app')
@section('content')

<div class="row">
   <div class="col-md-8 offset-md-2">
       <h3>Edit {{$listing->name}}</h3>

       <div >
           <img src="{{asset('storage/listings/'.$listing->photo)}}" class="fluid-img">
       </div>
       @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
   <form action="{{ route('admin.update.listings',['id'=>$listing->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="form-group ">

                    <label for="name" class="form-control-label">Name</label>

                    <input  id="name" class="form-control" name="name" type="text" value="{{$listing->name}}" placeholder="Property Name" />

                </div><!-- /.form-group -->

                <div class="form-group ">
                    <label for="location" class="form-control-label">Location</label>

                    <input name="location" value="{{$listing->location}}" type="text" id="location" class="form-control" placeholder="Property Location" />

                </div>


                <div class="form-group ">

                    <label for="bathroom" class="form-control-label">Bathroom</label>
                    <input name="bathroom" value="{{$listing->bathroom}}" type="number" id="bathroom" class="form-control" placeholder="Number of bathroom" />
                </div>
                <div class="form-group">
                    <label for="bedroom" class="form-control-label">Bedroom</label>
                    <input name="bedroom" value="{{$listing->bedroom}}" type="number" id="bedroom" class="form-control" placeholder="number of bedroom" />

                </div>

                <div class="form-group  ">
                    <label for="area" class="form-control-label">Area</label>

                    <input name="area" type="text" id="area" value="{{$listing->area}}" class="form-control" placeholder="Area in ft">

                </div>


                <div class=" form-group  ">
                    <label for="description" class="form-control-label">Description</label>

                    <textarea  rows="5" cols="40" name="description" id="description" class="form-control">{{$listing->description}}</textarea>

                </div>

                <div class="form-group  ">
                    <label for="cooling" class="form-control-label">Cooling</label>

                    <input type="text" name="cooling" placeholder="desc cooling" value="{{$listing->cooling}}" class="form-control">

                </div>



                <div class="form-group">
                    <label for="category" class="form-control-label">Select Category</label>
                    <select name="category_id" id="category" class="form-control">
                        @foreach($categories as $cat)
                            @if($listing->category_id == $cat->id)
                            <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                            @else
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="status" class="form-control-label">Select Status</label>
                    <select name="status" id="status" class="form-control">

                        <option value="1"  selected="{{$listing->status == 1 ? 'selected' : false }}">For Rent</option>
                        <option value="2" selected="{{$listing->status == 2 ? 'selected' : false }}">For Sale</option>

                    </select>
                </div>



                <div class="field_13 form-group  ">
                    <label for="price" class="form-control-label">Sales Price</label>
                   
                        <input type="text" name="price" placeholder="price in naira" value="{{$listing->price}}"  class="form-control">
                   
                </div>
            </div>
        </div>

        <hr class="mt15" />

        <div class="form-group">
            <label class="form-control-label">Picture</label>
            <input type="file" name="photo" class="form-control">

        </div>

        <div class="form-group row">
            <div class="col-sm-offset-4">
                <button type="submit" class=" btn btn-primary">Save</button>
            </div>
        </div>

    </form>
   </div>
</div>
@endsection