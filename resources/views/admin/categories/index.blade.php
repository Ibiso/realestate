@extends('layouts.admin.app')
@section('content')

<div class="row">
    <form method="POST" action="{{route('admin.store.listings-cat')}}">
        @csrf

        <div class="row">
            <div class="col-md-8">
                <input type="text" name="name" value="{{old('name')}}">
            </div>

            <div class="col-md-4">
                <input type="submit" class="btn btn-primary">
            </div>
        </div>
    </form>
</div>

<div class="row">
    <table class=" table table-responsive">
        <thead>
            <th>S/N</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>
        </thead>
        <tbody>
            @forelse($categories as $index=>$cat)
            <tr>
                <td>{{$index + 1}}</td>
                <td>{{$cat->name}}</td>
                <td>
                    <form method="POST" action="{{route('admin.update.listings-cat',['id'=>$cat->id])}}">
                        @csrf

                        <input type="text" name="name" class="" value="{{$cat->name}}">



                        <input type="submit" name="submit" class="btn btn-primary">

                    </form>
                </td>
                <td>
                    <form method="POST" action="{{route('admin.destroy.listings-cat',['id'=>$cat->id])}}">
                        @csrf
                        <input type="submit" class="btn btn-danger" name="submit" value="Delete">
                    </form>
                </td>
            </tr>
            @empty
            <p></p>
            @endforelse
        </tbody>
    </table>
</div>
@endsection