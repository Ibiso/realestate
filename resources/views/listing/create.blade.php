@extends('layouts.app')

@section('content')
<section class="pager-sec bfr">
    <div class="container">
        <div class="pager-sec-details">
            <h3>
                Quick submission
            </h3>
            <ul>
                <li class="item">
                    <a href="{{ url('/')}}">Home</a>
                </li>
            </ul>
        </div>
        <!--pager-sec-details end-->
    </div>
</section>
<div class="card uper">
    <div class="card-header" style="background-color:#070f43;">
        <h3 style="color: white;font-size: 26px; text-align: center;">Welcome to Ogarent</h3>
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <section class="blog-standard section-padding">
            <div class="container">
                <div class="blog-single-details">
                    <div class="row">
                        <div class="col-lg-8 ">
                            <div class="blog-posts">
                                <div id="post-10" class="blog-single-post  post-10 page type-page status-publish hentry">
                                    <div class="post_info">
                                        <ul class="post-nfo">
                                            <li><i class="la la-calendar"></i>September 26, 2019</li>
                                            <li><i class="la la-comment-o"></i><a href="index.html#comment-form" title="Comment">No Comments</a></li>
                                        </ul>
                                        <h3><span class="ntitle"></span></h3>
                                        <div class="post-content clearfix">
                                            <p></p>
                                            <div class="selio_sw_win_wrapper">
                                                <div class="ci sw_widget sw_wrap">
                                                    <div class="bootstrap-wrapper quick-submission">
                                                        <div class="widget widget-styles clearfix" id="quick_submission_form">


                                                            <form action="{{ route('listing.store') }}" method="POST">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-12">
                                                                        <div class="form-group  IS-INPUTBOX ">
                                                                           
                                                                            <label for="name" class="control-label">Name</label>
                                                                            <div class="form-field">
                                                                                <input class="" id="name" name="name" type="text" value="" placeholder="The name of your Property" />
                                                                            </div>
                                                                        </div><!-- /.form-group -->

                                                                        <div class="form-group  IS-INPUTBOX ">
                                                                            <label for="location" class="control-label">Location</label>
                                                                            <div class="form-field">
                                                                                <input name="location" value="" type="text" id="location" class="" placeholder="Location of the house" />
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group  IS-INPUTBOX ">
                                                                            <div class="form-field col-md-6">
                                                                                <label for="bathroom" class="control-label">Bathroom</label>
                                                                                <input name="bathroom" value="" type="number" id="bathroom" class="" placeholder="Number of bathroom" />
                                                                            </div>
                                                                            <div class="form-field col-md-6">
                                                                                <label for="bedroom" class="control-label">Bedroom</label>
                                                                                <input name="bedroom" value="" type="number" id="bedroom" class="" placeholder="number of bedroom" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group  ">
                                                                            <label for="area" class="control-label">Area</label>
                                                                            <div class="form-field">
                                                                                <input name="area" type="text" id="area" class="" placeholder="youar area in map">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="garage" class="control-label">Garage</label>
                                                                            <select class="form-field" name="garage">
                                                                                <option value="yes" name="yes" id="yes">Yes</option>
                                                                                <option value="no" name="no" id="yes">No</option>
                                                                            </select>
                                                                        </div>

                                                                        <div class=" form-group  ">
                                                                            <label for="description" class="control-label">Description</label>
                                                                            <div class="form-field">
                                                                                <textarea class="area" rows="5" cols="40" name="description" id="description"></textarea>
                                                                            </div>
                                                                        </div>

                                                                        <div class="field_13 form-group  ">
                                                                            <label for="cooling" class="control-label">Cooling</label>
                                                                            <div class="form-field">
                                                                                <input type="text" name="cooling" placeholder="What kind of cooling system is available">
                                                                            </div>
                                                                        </div>

                                                                        <div class="field_13 form-group  ">
                                                                            <label for="map" class="control-label">Map</label>
                                                                            <div class="form-field">
                                                                                <input type="text" name="map" placeholder="map description">
                                                                            </div>
                                                                        </div>

                                                                        <div class="field_1 ">
                                                                            <hr />
                                                                            <h4>Overview</h4>
                                                                            <hr />
                                                                        </div>

                                                                        <div class="field_13 form-group  ">
                                                                            <label for="price" class="control-label">Sales Price</label>
                                                                            <div class="form-field">
                                                                                <input type="text" name="price" placeholder="price in naira">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <hr class="mt15" />

                                                                <div class="fileupload-buttonbar celarfix" style="margin-top: 19px;">
                                                                    <div class="col-md-7">
                                                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                                                        <span class="btn btn-success fileinput-button">
                                                                            <i class="icon-plus icon-white"></i>
                                                                            <span>Add files...</span>
                                                                            <input type="file" name="photo">
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-sm-offset-4">
                                                                        <button type="submit" class="btn-default">Save</button>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-border">
                                <div class="category_header" style="background-color: #070f43; height: 48px;">
                                    <h1 style="text-align: center; color: white;">Featured Listing</h1>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- featured listing -->
    </div>





</div>
</div>
<p></p>
</div>
</div>
</div>
<!--blog-single-post end-->


</div>
<!--blog-posts end-->
</div>

</div>
</div>
<!--blog-single-details end-->
</div>
</section>
<!--blog-single-sec end-->
<div class="bottom-sidebar">
    <div id="widget-fullwidth-discover-1" class="widget-fullwidth-discover block-selio col-xl-3 col-sm-6 col-md-3"> <a href="#" title="Discover a home you&#8217;ll love to stay">
            <section class="cta st2 section-padding" style="background: #303e94;">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="cta-text">
                                <h2>Discover a home you&#8217;ll love to stay</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </a>

    </div> <!-- end widget -->
</div>




@endsection