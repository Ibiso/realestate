@extends('layout.app')xz/,

@section('content')

<!--  -->

<table class="table table-striped">
	<thead>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Location</td>
			<td>Bathroom</td>
			<td>Bedroom</td>
			<td>Area</td>
			<td>Garage</td>
			<td>Description</td>
			<td>Cooling</td>
			<td>Map</td>
			<td>Price</td>
			<td>Photo</td>
			<td colspan="2">Action</td>
		</tr>
	</thead>
<tbody>
@foreach ($listings as $listing)
<tr>
<td>{{ $listing->id }}</td>
<td>{{ $listing->name }}</td>
<td>{{ $listing->location }}</td>
<td>{{ $listing->bathroom }}</td>
<td>{{ $listing->bedroom }}</td>
<td>{{ $listing->area }}</td>
<td>{{ $listing->garage }}</td>
<td>{{ $listing->description }}</td>
<td>{{ $listing->cooling }}</td>
<td>{{ $listing->map }}</td>
<td>{{ $listing->price }}</td>
<td>{{ $listing->photo }}</td>
<td><a href="{{ route('listing.edit',$listing->id)}}"class="btn btn-primary">Edit</a></td>
<td>
<form action="{{ route('$listing.destroy',$listing->id)}}"method="post">
@csrf
@method('DELETE')
<buttonclass="btn btn-danger"type="submit">Delete</button>
</form>
</td>
</tr>
@endforeach
</tbody>
</table>
<div>
@endsection
