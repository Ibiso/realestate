<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\listing;
class ListingCategory extends Model
{
    //

    protected $guarded = [];

    public function listings()
    {
        return $this->hasMany(listing::class,'category_id');
    }
}
