<?php

namespace App\Http\Controllers;

use App\listing;
use App\ListingCategory;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getIndex(){
        return view('index');
    }
    public function getAbout(){
        return view('about');
    }
    public function getContact(){
        return view('contact');
    }
    public function getView(){
        $listings = listing::paginate(10);
        $categories = ListingCategory::all();

        return view('view',['listings'=>$listings,'categories'=>$categories]);
    }

    public function viewListing(Request $request)
    {
        dd('help yourself from here');
    }
}
