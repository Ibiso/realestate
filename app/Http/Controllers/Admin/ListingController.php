<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\listing as Listing;
use App\ListingCategory;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class ListingController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
        $this->storage = Storage::disk('listings');
    }


    public function index()
    {
        $listings = Listing::all();
        return view('admin.listings.index',['listings'=>$listings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ListingCategory::all();
        return view('admin.listings.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateListingsArray($request);
        $this->validate($request,[
            'photo'=>'required|file'
        ]);

        try {
            $filename = null;

            if($request->hasfile('photo') && $request->file('photo')->isValid()){
                $filename = $this->storage->putFile('',$request->file('photo'));
               
            }else{
                throw new \Exception(' You need to put a valid image to proceed');
            }

            $data = [
                'name' => $request->input('name'),
                'location' => $request->input('location'),
                'bathroom' => $request->input('bathroom'),
                'bedroom' => $request->input('bedroom'),
                'area' => $request->input('area'),
                'description' => $request->input('description'),
                'cooling' => $request->input('cooling'),
                'price' => $request->input('price'),
                'photo' => $filename,
                'status' => $request->input('status'),
                'category_id' => $request->input('category_id')
            ];
            Listing::forceCreate($data);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->back()->with('success', ' Listing successfully added');
    }

    public function validateListingsArray($request)
    {
        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'bathroom' => 'required',
            'bedroom' => 'required',
            'area' => 'required',
            'description' => 'required',
            'cooling' => 'required',
            'price' => 'required|integer',
            'category_id' => 'required'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing = Listing::find($id);

        return view('admin.listings.show', ['listing' => $listing]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listing = Listing::find($id);
        $categories = ListingCategory::all();
        return view('admin.listings.edit', ['listing' => $listing,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        //
        $this->validateListingsArray($request);
    
        try {
            $listing = Listing::find($request->id);
            $data = [
                'name' => $request->input('name'),
                'location' => $request->input('location'),
                'bathroom' => $request->input('bathroom'),
                'bedroom' => $request->input('bedroom'),
                'area' => $request->input('area'),
                'description' => $request->input('description'),
                'cooling' => $request->input('cooling'),
                'price' => $request->input('price'),
                'status' => $request->input('status'),
                'category_id' => $request->input('category_id')
            ];
           
            
            $listing->update($data);
            if($request->hasfile('photo') && $request->file('photo')->isValid()){
                $filename = $this->storage->putFile('',$request->file('photo'));
                $this->storage->delete($listing->photo);
                $listing->update(['photo'=>$filename]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->back()->with('success', ' Listing successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $listing = Listing::find($request->id);
        $this->storage->delete($listing->photo);
        $listing->delete();
        return redirect()->back()->with('success','Listing has been successfully been deleted');
    }
}
