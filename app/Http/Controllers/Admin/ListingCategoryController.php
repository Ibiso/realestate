<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ListingCategory;

class ListingCategoryController extends Controller
{
    //

    public function index()
    {
        $categories = ListingCategory::all();
        return view('admin.categories.index',['categories'=>$categories]);
    }


    public function show(Request $request)
    {
        $category = ListingCategory::find($request->id);
        return view('admin.categories.view',['category'=>$category]);
    }

   public function store(Request $request)
   {
       try{
        ListingCategory::create([
            'name'=>$request->name
        ]);
       }catch(\Exception $e){
        return redirect()->back()->with('error',$e->getMessage());
       }
       return redirect()->back()->with('success',' Category was successfully created');
   }

   


    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string'
        ]);

        $category = ListingCategory::find($request->id);
        $category->update([
            'name'=>$request->name
        ]);

        return redirect()->back()->with('success',' Category was successfully updated');

    }


    public function destroy(Request $request)
    {
        $category = ListingCategory::find($request->id);
        $category->delete();

        return redirect()->back()->with('success',' Category was successfully deleted');

    }


}
