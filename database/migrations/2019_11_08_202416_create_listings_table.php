<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location');
            $table->string('bathroom');
            $table->string('bedroom');
            $table->string('area');
            $table->longText('description');
            $table->string('cooling');
            //$table->string('map');
            $table->string('price');
            $table->string('photo');

            $table->integer('category_id');
            $table->integer('status')->comment('1 -For Rent, 2 - For Sale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
