<?php

use Faker\Generator as Faker;
use App\ListingCategory;

$factory->define(App\Listing::class, function (Faker $faker) {
    $categories = ListingCategory::all()->pluck('id')->toArray();
    $pics = [1,2,3,4,5,6];
    $status = [1,2];
    return [
        //
        'name'=>$faker->name,
        'location'=>$faker->city,
        'bathroom'=>$faker->randomDigit($nb = 1),
        'bedroom'=>$faker->randomDigit($nb = 1),
        'area'=>$faker->randomDigit($nb=3),
        'garage'=>$faker->randomDigit($nb=1),
        'description'=>$faker->paragraphs($nb = 3),
        'cooling'=>$faker->randomDigit(),
        'price'=>$faker->randomDigit($nb=5),
        'photo'=>$faker->randomElement($pics).'.jpg',
        'category_id'=>$faker->randomElement($categories),
        'status'=>$faker->randomElement($status)
    ];
});
