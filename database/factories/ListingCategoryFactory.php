<?php

use Faker\Generator as Faker;

$factory->define(App\ListingCategory::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->randomElement(['Homes','Apartments','Garages'])
    ];
});
